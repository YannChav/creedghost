package fr.creedghost.website.model;

import java.sql.Date;

import lombok.Data;

@Data
public class User {

	private Long id;

	private String username;

	private String firstname;

	private String lastname;

	private String email;

	private String password;

	private Date createdAt;

	private Date updatedAt;
}
