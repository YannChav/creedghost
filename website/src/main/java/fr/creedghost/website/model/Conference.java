package fr.creedghost.website.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Conference {

	private Long id;

	private String title;

	private String place;

	private Date conferenceDate;

	private String description;

	private Event event;

	private Date createdAt;

	private Date updatedAt;

	private List<Speaker> speakers = new ArrayList<>();
}
