package fr.creedghost.website.model;

import java.sql.Date;

import lombok.Data;

@Data
public class Stand {

	private Long id;

	private String title;

	private String place;

	private Date standDate;

	private String description;

	private Event event;

	private Date createdAt;

	private Date updatedAt;
}
