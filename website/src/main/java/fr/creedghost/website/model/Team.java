package fr.creedghost.website.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Team {

	private Long id;

	private String name;

	private Date createdAt;

	private Date updatedAt;

	private List<Player> players = new ArrayList<>();
}
