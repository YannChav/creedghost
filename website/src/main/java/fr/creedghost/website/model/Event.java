package fr.creedghost.website.model;

import java.sql.Date;

import lombok.Data;

@Data
public class Event {

	private Long id;

	private String name;

	private String description;

	private Date eventStart;

	private Date eventEnd;

	private Date createdAt;

	private Date updatedAt;
}
