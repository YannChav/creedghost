package fr.creedghost.website.model;

import java.sql.Date;

import lombok.Data;

@Data
public class CompetitionTeamTeam {

	private Long id;

	private Date registrationDate;

	private Team team;

	private int Ranking;

	private CompetitionTeam competition;
}
