package fr.creedghost.website.model;

import java.sql.Date;

import lombok.Data;

@Data
public class CompetitionSoloPlayer {

	private Long id;

	private Date registrationDate;

	private Player player;

	private int ranking;

	private CompetitionSolo competition;
}
