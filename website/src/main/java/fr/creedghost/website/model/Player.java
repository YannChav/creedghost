package fr.creedghost.website.model;

import java.sql.Date;

import lombok.Data;

@Data
public class Player {

	private Long id;

	private String username;

	private String firstname;

	private String lastname;

	private String cellphone;

	private String email;

	private String password;

	private Date birthday;

	private Date createdAt;

	private Date updatedAt;
}
