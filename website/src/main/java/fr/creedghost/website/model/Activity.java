package fr.creedghost.website.model;

import java.sql.Date;

import lombok.Data;

@Data
public class Activity {

	private Long id;

	private String title;

	private String place;

	private Date activityDate;

	private String description;

	private Event event;

	private Date createdAt;

	private Date updatedAt;
}
