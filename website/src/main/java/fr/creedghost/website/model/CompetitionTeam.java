package fr.creedghost.website.model;

import java.sql.Date;

import lombok.Data;

@Data
public class CompetitionTeam {

	private Long id;

	private String title;

	private Date competitionDate;

	private String description;

	private Event event;

	private Date createdAt;

	private Date updatedAt;
}
