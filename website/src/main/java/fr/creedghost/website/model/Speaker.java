package fr.creedghost.website.model;

import java.sql.Date;

import lombok.Data;

@Data
public class Speaker {

	private Long id;

	private String firstname;

	private String lastname;

	private String cellphone;

	private String email;

	private Date createdAt;

	private Date updatedAt;
}
