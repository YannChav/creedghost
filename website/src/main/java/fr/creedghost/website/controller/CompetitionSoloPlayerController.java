package fr.creedghost.website.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.CompetitionSolo;
import fr.creedghost.website.model.CompetitionSoloPlayer;
import fr.creedghost.website.model.Player;
import fr.creedghost.website.service.CompetitionSoloPlayerService;
import fr.creedghost.website.service.CompetitionSoloService;
import fr.creedghost.website.service.PlayerService;

@Controller
public class CompetitionSoloPlayerController {

	@Autowired
	private CompetitionSoloPlayerService competitionSoloPlayerService;
	@Autowired
	private CompetitionSoloService competitionSoloService;
	@Autowired
	private PlayerService playerService;

	@GetMapping("/admin/competition/solo/{idCompetition}/delete/player/{idPlayer}")
	public String deletePlayerInCompetitionSolo(@PathVariable("idCompetition") int idCompetition,
			@PathVariable("idPlayer") int idPlayer) {
		CompetitionSolo competitionSolo = competitionSoloService.getCompetitionSolo(idCompetition);

		competitionSoloPlayerService.deleteCompetitionSoloPlayer(idPlayer);

		return "redirect:/admin/event/" + competitionSolo.getEvent().getId() + "/update/competition/solo/"
				+ competitionSolo.getId();
	}

	@PostMapping("/admin/competition/solo/{idCompetition}/add/player")
	public ModelAndView addPlayerInCompetitionSolo(@ModelAttribute Player player,
			@PathVariable("idCompetition") int idCompetition) {
		CompetitionSolo competitionSolo = competitionSoloService.getCompetitionSolo(idCompetition);
		player = playerService.GetPlayerByUsername(player.getUsername());

		if (player == null) {
			return new ModelAndView("redirect:/admin/event/" + competitionSolo.getEvent().getId()
					+ "/update/competition/solo/" + competitionSolo.getId() + "/player-error");
		} else {
			CompetitionSoloPlayer competitionSoloPlayer = new CompetitionSoloPlayer();

			competitionSoloPlayer.setPlayer(player);
			competitionSoloPlayer.setCompetition(competitionSolo);
			competitionSoloPlayer.setRanking(0);
			competitionSoloPlayer.setRegistrationDate(new Date(System.currentTimeMillis()));

			competitionSoloPlayerService.saveCompetitionSoloPlayer(competitionSoloPlayer);

			return new ModelAndView("redirect:/admin/event/" + competitionSolo.getEvent().getId()
					+ "/update/competition/solo/" + competitionSolo.getId());
		}
	}

	@GetMapping("/admin/competition/solo/{idCompetition}/class/player/{idPlayer}")
	public String classPlayerInCompetitionSolo(Model model, @PathVariable("idCompetition") int idCompetition,
			@PathVariable("idPlayer") int idPlayer) {
		CompetitionSoloPlayer competitionSoloPlayer = competitionSoloPlayerService.getCompetitionSoloPlayer(idPlayer);
		model.addAttribute("player", competitionSoloPlayer);

		model.addAttribute("competition", idCompetition);

		return "admin/event/rank-player";
	}

	@PostMapping("/admin/competition/solo/{idCompetition}/class/player")
	public ModelAndView classPlayerInCompetitionSolo(@ModelAttribute CompetitionSoloPlayer competitionSoloPlayer,
			@PathVariable("idCompetition") int idCompetition) {
		CompetitionSolo competitionSolo = competitionSoloService.getCompetitionSolo(idCompetition);

		CompetitionSoloPlayer originalPlayer = competitionSoloPlayerService
				.getCompetitionSoloPlayer(competitionSoloPlayer.getId().intValue());

		originalPlayer.setRanking(competitionSoloPlayer.getRanking());

		competitionSoloPlayerService.saveCompetitionSoloPlayer(originalPlayer);

		return new ModelAndView("redirect:/admin/event/" + competitionSolo.getEvent().getId()
				+ "/update/competition/solo/" + competitionSolo.getId());
	}
}
