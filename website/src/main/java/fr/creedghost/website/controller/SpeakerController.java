package fr.creedghost.website.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.Player;
import fr.creedghost.website.model.Speaker;
import fr.creedghost.website.service.SpeakerService;

@Controller
public class SpeakerController {

	@Autowired
	private SpeakerService speakerService;

	@GetMapping("/admin/speaker/{id}/update")
	public String updateSpeaker(Model model, @PathVariable("id") int idSpeaker) {
		Speaker speaker = speakerService.getSpeaker(idSpeaker);
		model.addAttribute("speaker", speaker);

		model.addAttribute("update", true);

		return "admin/update-speaker";
	}

	@PostMapping("/admin/speaker/{id}/update")
	public ModelAndView updateSpeaker(@ModelAttribute Player speaker, @PathVariable("id") int id) {
		Speaker originalSpeaker = speakerService.getSpeaker(id);

		if (!speaker.getFirstname().equals(originalSpeaker.getFirstname()) && speaker.getFirstname() != "") {
			originalSpeaker.setFirstname(speaker.getFirstname());
		}
		if (!speaker.getLastname().equals(originalSpeaker.getLastname()) && speaker.getLastname() != "") {
			originalSpeaker.setLastname(speaker.getLastname());
		}
		if (!speaker.getEmail().equals(originalSpeaker.getEmail()) && speaker.getEmail() != "") {
			originalSpeaker.setEmail(speaker.getEmail());
		}
		if (!speaker.getCellphone().equals(originalSpeaker.getCellphone()) && speaker.getCellphone() != "") {
			originalSpeaker.setCellphone(speaker.getCellphone());
		}
		originalSpeaker.setUpdatedAt(new Date(System.currentTimeMillis()));

		speakerService.saveSpeaker(originalSpeaker);

		return new ModelAndView("redirect:/admin/dashboard");
	}

	@GetMapping("/admin/speaker/create")
	public String createSpeaker(Model model) {
		Speaker speaker = new Speaker();
		model.addAttribute("speaker", speaker);

		model.addAttribute("update", false);

		return "admin/update-speaker";
	}

	@PostMapping("/admin/speaker/create")
	public ModelAndView createSpeaker(@ModelAttribute Speaker speaker) {
		speaker.setCreatedAt(new Date(System.currentTimeMillis()));
		speaker.setUpdatedAt(new Date(System.currentTimeMillis()));

		speakerService.saveSpeaker(speaker);

		return new ModelAndView("redirect:/admin/dashboard");

	}

	@GetMapping("/admin/speaker/{id}/delete")
	public String deleteSpeaker(@PathVariable("id") int idSpeaker) {
		speakerService.deleteSpeaker(idSpeaker);

		return "redirect:/admin/dashboard";
	}
}
