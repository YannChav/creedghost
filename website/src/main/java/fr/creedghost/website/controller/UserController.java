package fr.creedghost.website.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.Event;
import fr.creedghost.website.model.Player;
import fr.creedghost.website.model.Speaker;
import fr.creedghost.website.model.Team;
import fr.creedghost.website.model.User;
import fr.creedghost.website.service.EventService;
import fr.creedghost.website.service.MD5Service;
import fr.creedghost.website.service.PlayerService;
import fr.creedghost.website.service.SpeakerService;
import fr.creedghost.website.service.TeamService;
import fr.creedghost.website.service.UserService;

@Controller
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private PlayerService playerService;
	@Autowired
	private TeamService teamService;
	@Autowired
	private EventService eventService;
	@Autowired
	private SpeakerService speakerService;

	@GetMapping("/admin")
	public String signin(Model model) {
		User user = new User();
		model.addAttribute("user", user);

		return "admin/signin";
	}

	@GetMapping("/admin/signin-error")
	public String signinError(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		model.addAttribute("loginError", true);

		return "admin/signin";
	}

	@GetMapping("/admin/logout")
	public String logout() {
		return "redirect:/";
	}

	@PostMapping("/admin/connection")
	public ModelAndView adminConnection(@ModelAttribute User user) {
		if (userService.verifiyUserExist(user.getUsername(), user.getPassword())) {
			return new ModelAndView("redirect:/admin/dashboard");
		} else {
			return new ModelAndView("redirect:/admin/signin-error");
		}
	}

	@GetMapping("/admin/dashboard")
	public String dashboard(Model model) {
		// Get all informations about data
		Iterable<User> listUsers = userService.getUsers();
		Iterable<Player> listPlayers = playerService.getPlayers();
		Iterable<Team> listTeams = teamService.getTeams();
		Iterable<Event> listEvents = eventService.getEvents();
		Iterable<Speaker> listSpeakers = speakerService.getSpeakers();
		// Put these informations to web page
		model.addAttribute("users", listUsers);
		model.addAttribute("players", listPlayers);
		model.addAttribute("teams", listTeams);
		model.addAttribute("events", listEvents);
		model.addAttribute("speakers", listSpeakers);

		return "admin/dashboard";
	}

	@GetMapping("/admin/user/{id}/update")
	public String updateUser(Model model, @PathVariable("id") int idUser) {
		User user = userService.getUser(idUser);
		user.setPassword("");
		model.addAttribute("user", user);

		model.addAttribute("update", true);

		return "admin/update-user";
	}

	@PostMapping("/admin/user/{id}/update")
	public ModelAndView updateUser(@ModelAttribute User user, @PathVariable("id") int id) {
		User originalUser = userService.getUser(id);

		if (!user.getUsername().equals(originalUser.getUsername()) && user.getUsername() != "") {
			originalUser.setUsername(user.getUsername());
		}
		if (!user.getFirstname().equals(originalUser.getFirstname()) && user.getFirstname() != "") {
			originalUser.setFirstname(user.getFirstname());
		}
		if (!user.getLastname().equals(originalUser.getLastname()) && user.getLastname() != "") {
			originalUser.setLastname(user.getLastname());
		}
		if (!user.getEmail().equals(originalUser.getEmail()) && user.getEmail() != "") {
			originalUser.setEmail(user.getEmail());
		}
		if (!MD5Service.getMd5(user.getPassword()).equals(originalUser.getUsername()) && user.getPassword() != "") {
			originalUser.setPassword(MD5Service.getMd5(user.getPassword()));
		}
		originalUser.setUpdatedAt(new Date(System.currentTimeMillis()));

		userService.saveUser(originalUser);

		return new ModelAndView("redirect:/admin/dashboard");
	}

	@GetMapping("/admin/user/create")
	public String createUser(Model model) {
		User user = new User();
		model.addAttribute("user", user);

		model.addAttribute("update", false);

		return "admin/update-user";
	}

	@PostMapping("/admin/user/create")
	public ModelAndView createUser(@ModelAttribute User user) {
		user.setPassword(MD5Service.getMd5(user.getPassword()));
		user.setCreatedAt(new Date(System.currentTimeMillis()));
		user.setUpdatedAt(new Date(System.currentTimeMillis()));

		userService.saveUser(user);

		return new ModelAndView("redirect:/admin/dashboard");

	}

	@GetMapping("/admin/user/{id}/delete")
	public String deleteUser(@PathVariable("id") int idUser) {
		userService.deleteUser(idUser);

		return "redirect:/admin/dashboard";
	}
}