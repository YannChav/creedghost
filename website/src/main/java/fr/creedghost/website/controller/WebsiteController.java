package fr.creedghost.website.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import fr.creedghost.website.model.Event;
import fr.creedghost.website.service.EventService;

@Controller
public class WebsiteController {

	@Autowired
	private EventService eventService;

	@GetMapping("/")
	public String home(Model model) {
		Iterable<Event> listEvents = eventService.getEvents();

		model.addAttribute("events", listEvents);
		return "home";
	}
}
