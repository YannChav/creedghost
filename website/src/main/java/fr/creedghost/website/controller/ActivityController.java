package fr.creedghost.website.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.Activity;
import fr.creedghost.website.model.Event;
import fr.creedghost.website.service.ActivityService;
import fr.creedghost.website.service.EventService;

@Controller
public class ActivityController {

	@Autowired
	private ActivityService activityService;
	@Autowired
	private EventService eventService;

	@GetMapping("/admin/event/{idEvent}/add/activity")
	public String eventAddActivity(Model model, @PathVariable("idEvent") int idEvent) {
		Activity activity = new Activity();
		model.addAttribute("activity", activity);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", false);

		return "admin/event/update-activity";
	}

	@PostMapping("/admin/event/{idEvent}/add/activity")
	public ModelAndView eventAddActivity(@ModelAttribute Activity activity, @PathVariable("idEvent") int idEvent) {
		Event event = eventService.getEvent(idEvent);

		// Create activity in database
		activity.setEvent(event);
		activity.setCreatedAt(new Date(System.currentTimeMillis()));
		activity.setUpdatedAt(new Date(System.currentTimeMillis()));
		activityService.saveActivity(activity);

		return new ModelAndView("redirect:/admin/event/" + event.getId() + "/update");
	}

	@GetMapping("/admin/event/{idEvent}/update/activity/{idActivity}")
	public String eventUpdateActivity(Model model, @PathVariable("idEvent") int idEvent,
			@PathVariable("idActivity") int idActivity) {
		Activity activity = activityService.getActivity(idActivity);
		model.addAttribute("activity", activity);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", true);

		return "admin/event/update-activity";
	}

	@PostMapping("/admin/event/{idEvent}/update/activity/{idActivity}")
	public ModelAndView eventUpdateActivity(@ModelAttribute Activity activity, @PathVariable("idEvent") int idEvent,
			@PathVariable("idActivity") int idActivity) {
		Activity originalActivity = activityService.getActivity(idActivity);

		originalActivity.setTitle(activity.getTitle());
		originalActivity.setDescription(activity.getDescription());
		originalActivity.setPlace(activity.getPlace());
		originalActivity.setActivityDate(activity.getActivityDate());

		originalActivity.setEvent(eventService.getEvent(idEvent));
		originalActivity.setUpdatedAt(new Date(System.currentTimeMillis()));

		activityService.saveActivity(originalActivity);

		return new ModelAndView("redirect:/admin/event/" + originalActivity.getEvent().getId() + "/update");
	}

	@GetMapping("/admin/event/{idEvent}/delete/activity/{idActivity}")
	public String eventDeletActivity(@PathVariable("idEvent") int idEvent, @PathVariable("idActivity") int idActivity) {
		activityService.deleteActivity(idActivity);

		return "redirect:/admin/event/" + idEvent + "/update";
	}

	@GetMapping("/activity")
	public String activities(Model model) {
		Iterable<Activity> listActivities = activityService.getActivities();
		model.addAttribute("activities", listActivities);

		return "pages/activity";
	}
}
