package fr.creedghost.website.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.Event;
import fr.creedghost.website.model.Stand;
import fr.creedghost.website.service.EventService;
import fr.creedghost.website.service.StandService;

@Controller
public class StandController {
	@Autowired
	private StandService standService;
	@Autowired
	private EventService eventService;

	@GetMapping("/admin/event/{idEvent}/add/stand")
	public String eventAddStand(Model model, @PathVariable("idEvent") int idEvent) {
		Stand stand = new Stand();
		model.addAttribute("stand", stand);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", false);

		return "admin/event/update-stand";
	}

	@PostMapping("/admin/event/{idEvent}/add/stand")
	public ModelAndView eventAddStand(@ModelAttribute Stand stand, @PathVariable("idEvent") int idEvent) {
		Event event = eventService.getEvent(idEvent);

		// Create stand in database
		stand.setEvent(event);
		stand.setCreatedAt(new Date(System.currentTimeMillis()));
		stand.setUpdatedAt(new Date(System.currentTimeMillis()));
		standService.saveStand(stand);

		return new ModelAndView("redirect:/admin/event/" + event.getId() + "/update");
	}

	@GetMapping("/admin/event/{idEvent}/update/stand/{idStand}")
	public String eventUpdateStand(Model model, @PathVariable("idEvent") int idEvent,
			@PathVariable("idStand") int idStand) {
		Stand stand = standService.getStand(idStand);
		model.addAttribute("stand", stand);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", true);

		return "admin/event/update-stand";
	}

	@PostMapping("/admin/event/{idEvent}/update/stand/{idStand}")
	public ModelAndView eventUpdateStand(@ModelAttribute Stand stand, @PathVariable("idEvent") int idEvent,
			@PathVariable("idStand") int idStand) {
		Stand originalStand = standService.getStand(idStand);

		originalStand.setTitle(stand.getTitle());
		originalStand.setDescription(stand.getDescription());
		originalStand.setPlace(stand.getPlace());
		originalStand.setStandDate(stand.getStandDate());

		originalStand.setEvent(eventService.getEvent(idEvent));
		originalStand.setUpdatedAt(new Date(System.currentTimeMillis()));

		standService.saveStand(originalStand);

		return new ModelAndView("redirect:/admin/event/" + originalStand.getEvent().getId() + "/update");
	}

	@GetMapping("/admin/event/{idEvent}/delete/stand/{idStand}")
	public String eventDeletStand(@PathVariable("idEvent") int idEvent, @PathVariable("idStand") int idStand) {
		standService.deleteStand(idStand);

		return "redirect:/admin/event/" + idEvent + "/update";
	}
}
