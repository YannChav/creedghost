package fr.creedghost.website.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.Activity;
import fr.creedghost.website.model.CompetitionSolo;
import fr.creedghost.website.model.CompetitionTeam;
import fr.creedghost.website.model.Conference;
import fr.creedghost.website.model.Event;
import fr.creedghost.website.model.Stand;
import fr.creedghost.website.service.ActivityService;
import fr.creedghost.website.service.CompetitionSoloService;
import fr.creedghost.website.service.CompetitionTeamService;
import fr.creedghost.website.service.ConferenceService;
import fr.creedghost.website.service.EventService;
import fr.creedghost.website.service.StandService;

@Controller
public class EventController {

	@Autowired
	private EventService eventService;
	@Autowired
	private ActivityService activityService;
	@Autowired
	private StandService standService;
	@Autowired
	private ConferenceService conferenceService;
	@Autowired
	private CompetitionSoloService competitionSoloService;
	@Autowired
	private CompetitionTeamService competitionTeamService;

	@GetMapping("/admin/event/{id}/update")
	public String event(Model model, @PathVariable("id") int idEvent) {
		Event event = eventService.getEvent(idEvent);
		// Get informations about event
		Iterable<Activity> listActivities = activityService.getActivitiesByEvent(idEvent);
		Iterable<Stand> listStands = standService.getStandsByEvent(idEvent);
		Iterable<Conference> listConferences = conferenceService.getConferencesByEvent(idEvent);
		Iterable<CompetitionSolo> listCompetitionsSolo = competitionSoloService.getCompetitionsSoloByEvent(idEvent);
		Iterable<CompetitionTeam> listCompetitionsTeam = competitionTeamService.getCompetitionsTeamByEvent(idEvent);
		// Put these informations to the view
		model.addAttribute("activities", listActivities);
		model.addAttribute("stands", listStands);
		model.addAttribute("conferences", listConferences);
		model.addAttribute("competitionsSolo", listCompetitionsSolo);
		model.addAttribute("competitionsTeam", listCompetitionsTeam);

		model.addAttribute(event);
		model.addAttribute("update", true);

		return "admin/event/update-event";
	}

	@PostMapping("/admin/event/{id}/update")
	public ModelAndView updateEvent(@ModelAttribute Event event, @PathVariable("id") int id) {
		Event originalEvent = eventService.getEvent(id);

		if (!event.getName().equals(originalEvent.getName()) && event.getName() != "") {
			originalEvent.setName(event.getName());
		}
		if (!event.getDescription().equals(originalEvent.getDescription()) && event.getDescription() != "") {
			originalEvent.setDescription(event.getDescription());
		}
		if (!event.getEventStart().equals(originalEvent.getEventStart()) && event.getEventStart() != null) {
			originalEvent.setEventStart(event.getEventStart());
		}
		if (!event.getEventEnd().equals(originalEvent.getEventEnd()) && event.getEventEnd() != null) {
			originalEvent.setEventEnd(event.getEventEnd());
		}
		originalEvent.setUpdatedAt(new Date(System.currentTimeMillis()));

		eventService.saveEvent(originalEvent);

		return new ModelAndView("redirect:/admin/dashboard");
	}

	@GetMapping("/admin/event/create")
	public String createEvent(Model model) {
		Event event = new Event();
		model.addAttribute("event", event);

		model.addAttribute("update", false);

		return "admin/event/update-event";
	}

	@PostMapping("/admin/event/create")
	public ModelAndView createEvent(@ModelAttribute Event event) {
		event.setCreatedAt(new Date(System.currentTimeMillis()));
		event.setUpdatedAt(new Date(System.currentTimeMillis()));

		eventService.saveEvent(event);

		return new ModelAndView("redirect:/admin/dashboard");

	}

	@GetMapping("/admin/event/{id}/delete")
	public String deleteEvent(@PathVariable("id") int idEvent) {
		eventService.deleteEvent(idEvent);

		return "redirect:/admin/dashboard";
	}

	@GetMapping("/event/{id}")
	public String getEventVisitor(Model model, @PathVariable("id") int idEvent) {
		Event event = eventService.getEvent(idEvent);
		model.addAttribute("event", event);

		Iterable<Activity> listActivities = activityService.getActivitiesByEvent(idEvent);
		Iterable<Stand> listStands = standService.getStandsByEvent(idEvent);
		Iterable<Conference> listConferences = conferenceService.getConferencesByEvent(idEvent);
		Iterable<CompetitionSolo> listCompetitionsSolo = competitionSoloService.getCompetitionsSoloByEvent(idEvent);
		Iterable<CompetitionTeam> listCompetitionsTeam = competitionTeamService.getCompetitionsTeamByEvent(idEvent);
		model.addAttribute("activities", listActivities);
		model.addAttribute("stands", listStands);
		model.addAttribute("conferences", listConferences);
		model.addAttribute("competitionsSolo", listCompetitionsSolo);
		model.addAttribute("competitionsTeam", listCompetitionsTeam);

		return "event";
	}
}
