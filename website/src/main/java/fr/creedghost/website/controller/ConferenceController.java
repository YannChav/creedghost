package fr.creedghost.website.controller;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.Conference;
import fr.creedghost.website.model.Event;
import fr.creedghost.website.model.Speaker;
import fr.creedghost.website.service.ConferenceService;
import fr.creedghost.website.service.EventService;
import fr.creedghost.website.service.SpeakerService;

@Controller
public class ConferenceController {

	@Autowired
	private ConferenceService conferenceService;
	@Autowired
	private EventService eventService;
	@Autowired
	private SpeakerService speakerService;

	@GetMapping("/admin/event/{idEvent}/add/conference")
	public String eventAddConference(Model model, @PathVariable("idEvent") int idEvent) {
		Conference conference = new Conference();
		model.addAttribute("conference", conference);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", false);

		return "admin/event/update-conference";
	}

	@PostMapping("/admin/event/{idEvent}/add/conference")
	public ModelAndView eventAddConference(@ModelAttribute Conference conference,
			@PathVariable("idEvent") int idEvent) {
		Event event = eventService.getEvent(idEvent);

		// Create conference in database
		conference.setEvent(event);
		conference.setCreatedAt(new Date(System.currentTimeMillis()));
		conference.setUpdatedAt(new Date(System.currentTimeMillis()));
		conferenceService.saveConference(conference);

		return new ModelAndView("redirect:/admin/event/" + event.getId() + "/update");
	}

	@GetMapping("/admin/event/{idEvent}/update/conference/{idConference}")
	public String eventUpdateConference(Model model, @PathVariable("idEvent") int idEvent,
			@PathVariable("idConference") int idConference) {
		Conference conference = conferenceService.getConference(idConference);
		model.addAttribute("conference", conference);
		Speaker speaker = new Speaker();
		model.addAttribute("speaker", speaker);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", true);
		model.addAttribute("speakerError", false);

		return "admin/event/update-conference";
	}

	@GetMapping("/admin/event/{idEvent}/update/conference/{idConference}/speaker-error")
	public String eventUpdateConferenceSpeakerError(Model model, @PathVariable("idEvent") int idEvent,
			@PathVariable("idConference") int idConference) {
		Conference conference = conferenceService.getConference(idConference);
		model.addAttribute("conference", conference);
		Speaker speaker = new Speaker();
		model.addAttribute("speaker", speaker);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", true);
		model.addAttribute("speakerError", true);

		return "admin/event/update-conference";
	}

	@PostMapping("/admin/event/{idEvent}/update/conference/{idConference}")
	public ModelAndView eventUpdateConference(@ModelAttribute Conference conference,
			@PathVariable("idEvent") int idEvent, @PathVariable("idConference") int idConference) {
		Conference originalConference = conferenceService.getConference(idConference);

		originalConference.setTitle(conference.getTitle());
		originalConference.setDescription(conference.getDescription());
		originalConference.setPlace(conference.getPlace());
		originalConference.setConferenceDate(conference.getConferenceDate());

		originalConference.setEvent(eventService.getEvent(idEvent));
		originalConference.setUpdatedAt(new Date(System.currentTimeMillis()));

		conferenceService.saveConference(originalConference);

		return new ModelAndView("redirect:/admin/event/" + originalConference.getEvent().getId() + "/update");
	}

	@GetMapping("/admin/event/{idEvent}/delete/conference/{idConference}")
	public String eventDeletConference(@PathVariable("idEvent") int idEvent,
			@PathVariable("idConference") int idConference) {
		conferenceService.deleteConference(idConference);

		return "redirect:/admin/event/" + idEvent + "/update";
	}

	@GetMapping("/admin/conference/{idConference}/delete/speaker/{idSpeaker}")
	public String deleteSpeakerInConference(@PathVariable("idConference") int idConference,
			@PathVariable("idSpeaker") int idSpeaker) {
		Conference conference = conferenceService.getConference(idConference);
		Speaker speaker = speakerService.getSpeaker(idSpeaker);

		List<Speaker> speakers = conference.getSpeakers();
		speakers.remove(speaker);

		conference.setSpeakers(speakers);
		conferenceService.saveConference(conference);

		return "redirect:/admin/event/" + conference.getEvent().getId() + "/update/conference/" + conference.getId();
	}

	@PostMapping("/admin/conference/{idConference}/add/speaker")
	public ModelAndView addSpeakerInConference(@ModelAttribute Speaker speaker,
			@PathVariable("idConference") int idConference) {
		Conference conference = conferenceService.getConference(idConference);
		speaker = speakerService.getSpeakerByEmail(speaker.getEmail());

		if (speaker == null) {
			return new ModelAndView("redirect:/admin/event/" + conference.getEvent().getId() + "/update/conference/"
					+ conference.getId() + "/speaker-error");
		} else {
			List<Speaker> speakers = conference.getSpeakers();
			speakers.add(speaker);
			conference.setSpeakers(speakers);

			conferenceService.saveConference(conference);

			return new ModelAndView("redirect:/admin/event/" + conference.getEvent().getId() + "/update/conference/"
					+ conference.getId());
		}
	}

	@GetMapping("/event/{idEvent}/conference/{idConference}")
	public String getConferenceVisitor(Model model, @PathVariable("idConference") int idConference) {
		Conference conference = conferenceService.getConference(idConference);
		model.addAttribute("conference", conference);

		return "conference";
	}

}
