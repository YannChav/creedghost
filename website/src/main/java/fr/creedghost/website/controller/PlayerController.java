package fr.creedghost.website.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.Player;
import fr.creedghost.website.service.MD5Service;
import fr.creedghost.website.service.PlayerService;

@Controller
public class PlayerController {

	@Autowired
	private PlayerService playerService;

	@GetMapping("/player/signin")
	public String signin(Model model) {
		Player player = new Player();
		model.addAttribute("player", player);

		return "player/signin";
	}

	@GetMapping("/player/signin-error")
	public String signinError(Model model) {
		Player player = new Player();
		model.addAttribute("player", player);
		model.addAttribute("loginError", true);

		return "player/signin";
	}

	@PostMapping("/player/connection")
	public ModelAndView playerConnect(@ModelAttribute Player player) {
		if (playerService.verifiyPlayerExist(player.getUsername(), player.getPassword())) {
			return new ModelAndView(
					"redirect:/player/" + playerService.GetPlayerByUsername(player.getUsername()).getId() + "/profil");
		} else {
			return new ModelAndView("redirect:/player/signin-error");
		}
	}

	@GetMapping("/player/signup")
	public String signup(Model model) {
		Player player = new Player();
		model.addAttribute("player", player);

		model.addAttribute("registrationError", false);

		return "player/signup";
	}

	@GetMapping("/player/signup/error")
	public String signupError(Model model) {
		Player player = new Player();
		model.addAttribute("player", player);

		model.addAttribute("registrationError", true);

		return "player/signup";
	}

	@PostMapping("/player/signup")
	public ModelAndView signup(@ModelAttribute Player player) {
		if (playerService.GetPlayerByUsername(player.getUsername()) == null) {
			player.setPassword(MD5Service.getMd5(player.getPassword()));
			player.setCreatedAt(new Date(System.currentTimeMillis()));
			player.setUpdatedAt(new Date(System.currentTimeMillis()));

			player = playerService.savePlayer(player);
			return new ModelAndView("redirect:/player/" + player.getId() + "/profil");
		} else {
			return new ModelAndView("redirect:/player/signup/error");
		}
	}

	@GetMapping("/player/logout")
	public String signup() {
		return "redirect:/";
	}

	@GetMapping("/player/{id}/profil")
	public String playerProfil(Model model, @PathVariable("id") int idPlayer) {
		Player player = playerService.getPlayer(idPlayer);
		model.addAttribute("player", player);

		return "player/profil";
	}

	@GetMapping("/admin/player/{id}/update")
	public String updatePlayer(Model model, @PathVariable("id") int idPlayer) {
		Player player = playerService.getPlayer(idPlayer);
		player.setPassword("");
		model.addAttribute("player", player);

		model.addAttribute("update", true);

		return "admin/update-player";
	}

	@PostMapping("/admin/player/{id}/update")
	public ModelAndView updatePlayer(@ModelAttribute Player player, @PathVariable("id") int id) {
		Player originalPlayer = playerService.getPlayer(id);

		if (!player.getUsername().equals(originalPlayer.getUsername()) && player.getUsername() != "") {
			originalPlayer.setUsername(player.getUsername());
		}
		if (!player.getFirstname().equals(originalPlayer.getFirstname()) && player.getFirstname() != "") {
			originalPlayer.setFirstname(player.getFirstname());
		}
		if (!player.getLastname().equals(originalPlayer.getLastname()) && player.getLastname() != "") {
			originalPlayer.setLastname(player.getLastname());
		}
		if (!player.getEmail().equals(originalPlayer.getEmail()) && player.getEmail() != "") {
			originalPlayer.setEmail(player.getEmail());
		}
		if (!player.getCellphone().equals(originalPlayer.getCellphone()) && player.getCellphone() != "") {
			originalPlayer.setCellphone(player.getCellphone());
		}
		if (!player.getBirthday().equals(originalPlayer.getBirthday()) && player.getBirthday() != null) {
			originalPlayer.setBirthday(player.getBirthday());
		}
		if (!MD5Service.getMd5(player.getPassword()).equals(originalPlayer.getUsername())
				&& player.getPassword() != "") {
			originalPlayer.setPassword(MD5Service.getMd5(player.getPassword()));
		}
		originalPlayer.setUpdatedAt(new Date(System.currentTimeMillis()));

		playerService.savePlayer(originalPlayer);

		return new ModelAndView("redirect:/admin/dashboard");
	}

	@GetMapping("/admin/player/create")
	public String createPlayer(Model model) {
		Player player = new Player();
		model.addAttribute("player", player);

		model.addAttribute("update", false);

		return "admin/update-player";
	}

	@PostMapping("/admin/player/create")
	public ModelAndView createPlayer(@ModelAttribute Player player) {
		player.setPassword(MD5Service.getMd5(player.getPassword()));
		player.setCreatedAt(new Date(System.currentTimeMillis()));
		player.setUpdatedAt(new Date(System.currentTimeMillis()));

		playerService.savePlayer(player);

		return new ModelAndView("redirect:/admin/dashboard");

	}

	@GetMapping("/admin/player/{id}/delete")
	public String deletePlayer(@PathVariable("id") int idPlayer) {
		playerService.deletePlayer(idPlayer);

		return "redirect:/admin/dashboard";
	}
}
