package fr.creedghost.website.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.CompetitionTeam;
import fr.creedghost.website.model.CompetitionTeamTeam;
import fr.creedghost.website.model.Event;
import fr.creedghost.website.model.Team;
import fr.creedghost.website.service.CompetitionTeamService;
import fr.creedghost.website.service.CompetitionTeamTeamService;
import fr.creedghost.website.service.EventService;

@Controller
public class CompetitionTeamController {

	@Autowired
	private CompetitionTeamService competitionTeamService;
	@Autowired
	private CompetitionTeamTeamService competitionTeamTeamService;
	@Autowired
	private EventService eventService;

	@GetMapping("/admin/event/{idEvent}/add/competition/team")
	public String eventAddCompetitionTeam(Model model, @PathVariable("idEvent") int idEvent) {
		CompetitionTeam competitionTeam = new CompetitionTeam();
		model.addAttribute("competition", competitionTeam);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", false);

		return "admin/event/update-competition-team";
	}

	@PostMapping("/admin/event/{idEvent}/add/competition/team")
	public ModelAndView eventAddCompetitionTeam(@ModelAttribute CompetitionTeam competitionTeam,
			@PathVariable("idEvent") int idEvent) {
		Event event = eventService.getEvent(idEvent);

		// Create conference in database
		competitionTeam.setEvent(event);
		competitionTeam.setCreatedAt(new Date(System.currentTimeMillis()));
		competitionTeam.setUpdatedAt(new Date(System.currentTimeMillis()));
		competitionTeamService.saveCompetitionTeam(competitionTeam);

		return new ModelAndView("redirect:/admin/event/" + event.getId() + "/update");
	}

	@GetMapping("/admin/event/{idEvent}/update/competition/team/{idCompetition}")
	public String eventUpdateCompetitionTeam(Model model, @PathVariable("idEvent") int idEvent,
			@PathVariable("idCompetition") int idCompetition) {
		CompetitionTeam competitionTeam = competitionTeamService.getCompetitionTeam(idCompetition);
		model.addAttribute("competition", competitionTeam);
		Iterable<CompetitionTeamTeam> teams = competitionTeamTeamService
				.getCompetitionTeamTeamsByCompetition(idCompetition);
		model.addAttribute("competitionTeams", teams);
		Team team = new Team();
		model.addAttribute("team", team);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", true);
		model.addAttribute("teamError", false);

		return "admin/event/update-competition-team";
	}

	@GetMapping("/admin/event/{idEvent}/update/competition/team/{idCompetition}/team-error")
	public String eventUpdateCompetitionTeamTeamError(Model model, @PathVariable("idEvent") int idEvent,
			@PathVariable("idCompetition") int idCompetition) {
		CompetitionTeam competitionTeam = competitionTeamService.getCompetitionTeam(idCompetition);
		model.addAttribute("competition", competitionTeam);
		Iterable<CompetitionTeamTeam> teams = competitionTeamTeamService
				.getCompetitionTeamTeamsByCompetition(idCompetition);
		model.addAttribute("competitionTeams", teams);
		Team team = new Team();
		model.addAttribute("team", team);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", true);
		model.addAttribute("teamError", true);

		return "admin/event/update-competition-team";
	}

	@PostMapping("/admin/event/{idEvent}/update/competition/team/{idCompetition}")
	public ModelAndView eventUpdateCompetitionTeam(@ModelAttribute CompetitionTeam competitionTeam,
			@PathVariable("idEvent") int idEvent, @PathVariable("idCompetition") int idCompetition) {
		CompetitionTeam originalCompetition = competitionTeamService.getCompetitionTeam(idCompetition);

		originalCompetition.setTitle(competitionTeam.getTitle());
		originalCompetition.setDescription(competitionTeam.getDescription());
		originalCompetition.setCompetitionDate(competitionTeam.getCompetitionDate());

		originalCompetition.setEvent(eventService.getEvent(idEvent));
		originalCompetition.setUpdatedAt(new Date(System.currentTimeMillis()));

		competitionTeamService.saveCompetitionTeam(originalCompetition);

		return new ModelAndView("redirect:/admin/event/" + originalCompetition.getEvent().getId() + "/update");
	}

	@GetMapping("/admin/event/{idEvent}/delete/competition/team/{idCompetition}")
	public String eventDeleteCompetitionTeam(@PathVariable("idEvent") int idEvent,
			@PathVariable("idCompetition") int idCompetition) {
		competitionTeamService.deleteCompetitionTeam(idCompetition);

		return "redirect:/admin/event/" + idEvent + "/update";
	}

	@GetMapping("/event/{idEvent}/competition/team/{idCompetition}")
	public String getCompetitionSoloVisitor(Model model, @PathVariable("idCompetition") int idCompetition) {
		CompetitionTeam competitionTeam = competitionTeamService.getCompetitionTeam(idCompetition);
		model.addAttribute("competition", competitionTeam);
		Iterable<CompetitionTeamTeam> teams = competitionTeamTeamService
				.getCompetitionTeamTeamsByCompetition(idCompetition);
		model.addAttribute("competitionTeams", teams);

		return "competition-team";
	}
}
