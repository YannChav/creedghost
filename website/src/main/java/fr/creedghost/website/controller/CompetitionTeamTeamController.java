package fr.creedghost.website.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.CompetitionTeam;
import fr.creedghost.website.model.CompetitionTeamTeam;
import fr.creedghost.website.model.Team;
import fr.creedghost.website.service.CompetitionTeamService;
import fr.creedghost.website.service.CompetitionTeamTeamService;
import fr.creedghost.website.service.TeamService;

@Controller
public class CompetitionTeamTeamController {

	@Autowired
	private CompetitionTeamTeamService competitionTeamTeamService;
	@Autowired
	private CompetitionTeamService competitionTeamService;
	@Autowired
	private TeamService teamService;

	@GetMapping("/admin/competition/team/{idCompetition}/delete/team/{idTeam}")
	public String deleteTeamInCompetitionTeam(@PathVariable("idCompetition") int idCompetition,
			@PathVariable("idTeam") int idTeam) {
		CompetitionTeam competitionTeam = competitionTeamService.getCompetitionTeam(idCompetition);

		competitionTeamTeamService.deleteCompetitionTeamTeam(idTeam);

		return "redirect:/admin/event/" + competitionTeam.getEvent().getId() + "/update/competition/team/"
				+ competitionTeam.getId();
	}

	@PostMapping("/admin/competition/team/{idCompetition}/add/team")
	public ModelAndView addTeamInCompetitionTeam(@ModelAttribute Team team,
			@PathVariable("idCompetition") int idCompetition) {
		CompetitionTeam competitionTeam = competitionTeamService.getCompetitionTeam(idCompetition);
		team = teamService.getTeamByName(team.getName());

		if (team == null) {
			return new ModelAndView("redirect:/admin/event/" + competitionTeam.getEvent().getId()
					+ "/update/competition/team/" + competitionTeam.getId() + "/team-error");
		} else {
			CompetitionTeamTeam competitionTeamTeam = new CompetitionTeamTeam();

			competitionTeamTeam.setTeam(team);
			competitionTeamTeam.setCompetition(competitionTeam);
			competitionTeamTeam.setRanking(0);
			competitionTeamTeam.setRegistrationDate(new Date(System.currentTimeMillis()));

			competitionTeamTeamService.saveCompetitionTeamTeam(competitionTeamTeam);

			return new ModelAndView("redirect:/admin/event/" + competitionTeam.getEvent().getId()
					+ "/update/competition/team/" + competitionTeam.getId());
		}
	}

	@GetMapping("/admin/competition/team/{idCompetition}/class/team/{idTeam}")
	public String classTeamInCompetitionTeam(Model model, @PathVariable("idCompetition") int idCompetition,
			@PathVariable("idTeam") int idTeam) {
		CompetitionTeamTeam competitionTeamTeam = competitionTeamTeamService.getCompetitionTeamTeam(idTeam);
		model.addAttribute("team", competitionTeamTeam);

		model.addAttribute("competition", idCompetition);

		return "admin/event/rank-team";
	}

	@PostMapping("/admin/competition/team/{idCompetition}/class/team")
	public ModelAndView classTeamInCompetitionTeam(@ModelAttribute CompetitionTeamTeam competitionTeamTeam,
			@PathVariable("idCompetition") int idCompetition) {
		CompetitionTeam competitionTeam = competitionTeamService.getCompetitionTeam(idCompetition);

		CompetitionTeamTeam originalTeam = competitionTeamTeamService
				.getCompetitionTeamTeam(competitionTeamTeam.getId().intValue());

		originalTeam.setRanking(competitionTeamTeam.getRanking());

		competitionTeamTeamService.saveCompetitionTeamTeam(originalTeam);

		return new ModelAndView("redirect:/admin/event/" + competitionTeam.getEvent().getId()
				+ "/update/competition/team/" + competitionTeam.getId());
	}
}
