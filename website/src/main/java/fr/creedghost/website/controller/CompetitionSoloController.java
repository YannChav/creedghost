package fr.creedghost.website.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.CompetitionSolo;
import fr.creedghost.website.model.CompetitionSoloPlayer;
import fr.creedghost.website.model.Event;
import fr.creedghost.website.model.Player;
import fr.creedghost.website.service.CompetitionSoloPlayerService;
import fr.creedghost.website.service.CompetitionSoloService;
import fr.creedghost.website.service.EventService;

@Controller
public class CompetitionSoloController {

	@Autowired
	private CompetitionSoloService competitionSoloService;
	@Autowired
	private CompetitionSoloPlayerService competitionSoloPlayerService;
	@Autowired
	private EventService eventService;

	@GetMapping("/admin/event/{idEvent}/add/competition/solo")
	public String eventAddCompetitionSolo(Model model, @PathVariable("idEvent") int idEvent) {
		CompetitionSolo competitionSolo = new CompetitionSolo();
		model.addAttribute("competition", competitionSolo);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", false);

		return "admin/event/update-competition-solo";
	}

	@PostMapping("/admin/event/{idEvent}/add/competition/solo")
	public ModelAndView eventAddCompetitionSolo(@ModelAttribute CompetitionSolo competitionSolo,
			@PathVariable("idEvent") int idEvent) {
		Event event = eventService.getEvent(idEvent);

		// Create conference in database
		competitionSolo.setEvent(event);
		competitionSolo.setCreatedAt(new Date(System.currentTimeMillis()));
		competitionSolo.setUpdatedAt(new Date(System.currentTimeMillis()));
		competitionSoloService.saveCompetitionSolo(competitionSolo);

		return new ModelAndView("redirect:/admin/event/" + event.getId() + "/update");
	}

	@GetMapping("/admin/event/{idEvent}/update/competition/solo/{idCompetition}")
	public String eventUpdateCompetitionSolo(Model model, @PathVariable("idEvent") int idEvent,
			@PathVariable("idCompetition") int idCompetition) {
		CompetitionSolo competitionSolo = competitionSoloService.getCompetitionSolo(idCompetition);
		model.addAttribute("competition", competitionSolo);
		Iterable<CompetitionSoloPlayer> players = competitionSoloPlayerService
				.getCompetitionSoloPlayersByCompetition(idCompetition);
		model.addAttribute("competitionPlayers", players);
		Player player = new Player();
		model.addAttribute("player", player);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", true);
		model.addAttribute("playerError", false);

		return "admin/event/update-competition-solo";
	}

	@GetMapping("/admin/event/{idEvent}/update/competition/solo/{idCompetition}/player-error")
	public String eventUpdateCompetitionSoloPlayerError(Model model, @PathVariable("idEvent") int idEvent,
			@PathVariable("idCompetition") int idCompetition) {
		CompetitionSolo competitionSolo = competitionSoloService.getCompetitionSolo(idCompetition);
		model.addAttribute("competition", competitionSolo);
		Iterable<CompetitionSoloPlayer> players = competitionSoloPlayerService
				.getCompetitionSoloPlayersByCompetition(idCompetition);
		model.addAttribute("competitionPlayers", players);
		Player player = new Player();
		model.addAttribute("player", player);

		model.addAttribute("event", idEvent);
		model.addAttribute("update", true);
		model.addAttribute("playerError", true);

		return "admin/event/update-competition-solo";
	}

	@PostMapping("/admin/event/{idEvent}/update/competition/solo/{idCompetition}")
	public ModelAndView eventUpdateCompetitionSolo(@ModelAttribute CompetitionSolo competitionSolo,
			@PathVariable("idEvent") int idEvent, @PathVariable("idCompetition") int idCompetition) {
		CompetitionSolo originalCompetition = competitionSoloService.getCompetitionSolo(idCompetition);

		originalCompetition.setTitle(competitionSolo.getTitle());
		originalCompetition.setDescription(competitionSolo.getDescription());
		originalCompetition.setCompetitionDate(competitionSolo.getCompetitionDate());

		originalCompetition.setEvent(eventService.getEvent(idEvent));
		originalCompetition.setUpdatedAt(new Date(System.currentTimeMillis()));

		competitionSoloService.saveCompetitionSolo(originalCompetition);

		return new ModelAndView("redirect:/admin/event/" + originalCompetition.getEvent().getId() + "/update");
	}

	@GetMapping("/admin/event/{idEvent}/delete/competition/solo/{idCompetition}")
	public String eventDeleteCompetitionSolo(@PathVariable("idEvent") int idEvent,
			@PathVariable("idCompetition") int idCompetition) {
		competitionSoloService.deleteCompetitionSolo(idCompetition);

		return "redirect:/admin/event/" + idEvent + "/update";
	}

	@GetMapping("/event/{idEvent}/competition/solo/{idCompetition}")
	public String getCompetitionSoloVisitor(Model model, @PathVariable("idCompetition") int idCompetition) {
		CompetitionSolo competitionSolo = competitionSoloService.getCompetitionSolo(idCompetition);
		model.addAttribute("competition", competitionSolo);
		Iterable<CompetitionSoloPlayer> players = competitionSoloPlayerService
				.getCompetitionSoloPlayersByCompetition(idCompetition);
		model.addAttribute("competitionPlayers", players);

		return "competition-solo";
	}
}
