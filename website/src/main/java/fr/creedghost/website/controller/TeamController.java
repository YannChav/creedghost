package fr.creedghost.website.controller;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import fr.creedghost.website.model.Player;
import fr.creedghost.website.model.Team;
import fr.creedghost.website.service.PlayerService;
import fr.creedghost.website.service.TeamService;

@Controller
public class TeamController {

	@Autowired
	private TeamService teamService;

	@Autowired
	private PlayerService playerService;

	@GetMapping("/admin/team/{id}/update")
	public String updateTeam(Model model, @PathVariable("id") int idTeam) {
		Team team = teamService.getTeam(idTeam);
		model.addAttribute("team", team);
		Iterable<Player> players = playerService.getPlayers();
		model.addAttribute("players", players);

		Player playerToAdd = new Player();
		model.addAttribute("player", playerToAdd);

		model.addAttribute("update", true);
		model.addAttribute("playerError", false);

		return "admin/update-team";
	}

	@GetMapping("/admin/team/{id}/update/player-error")
	public String updateTeamErrorPlayer(Model model, @PathVariable("id") int idTeam) {
		Team team = teamService.getTeam(idTeam);
		model.addAttribute("team", team);
		Iterable<Player> players = playerService.getPlayers();
		model.addAttribute("players", players);
		Player playerToAdd = new Player();
		model.addAttribute("player", playerToAdd);

		model.addAttribute("update", true);
		model.addAttribute("playerError", true);

		return "admin/update-team";
	}

	@PostMapping("/admin/team/{id}/update")
	public ModelAndView updateTeam(@ModelAttribute Team team, @PathVariable("id") int id) {
		Team originalTeam = teamService.getTeam(id);

		if (!team.getName().equals(originalTeam.getName()) && team.getName() != "") {
			originalTeam.setName(team.getName());
		}
		originalTeam.setUpdatedAt(new Date(System.currentTimeMillis()));

		teamService.saveTeam(originalTeam);

		return new ModelAndView("redirect:/admin/dashboard");
	}

	@GetMapping("/admin/team/create")
	public String createTeam(Model model) {
		Team team = new Team();
		model.addAttribute("team", team);

		model.addAttribute("update", false);

		return "admin/update-team";
	}

	@PostMapping("/admin/team/create")
	public ModelAndView createTeam(@ModelAttribute Team team) {
		team.setCreatedAt(new Date(System.currentTimeMillis()));
		team.setUpdatedAt(new Date(System.currentTimeMillis()));

		teamService.saveTeam(team);

		return new ModelAndView("redirect:/admin/dashboard");
	}

	@GetMapping("/admin/team/{idTeam}/delete/player/{idPlayer}")
	public String deletePlayerInTeam(@PathVariable("idTeam") int idTeam, @PathVariable("idPlayer") int idPlayer) {
		Team team = teamService.getTeam(idTeam);
		Player player = playerService.getPlayer(idPlayer);

		List<Player> players = team.getPlayers();
		players.remove(player);

		team.setPlayers(players);
		teamService.saveTeam(team);

		return "redirect:/admin/team/" + idTeam + "/update";
	}

	@PostMapping("/admin/team/{idTeam}/add/player")
	public ModelAndView addPlayerInTeam(@ModelAttribute Player player, @PathVariable("idTeam") int idTeam) {
		player = playerService.GetPlayerByUsername(player.getUsername());

		if (player == null) {
			return new ModelAndView("redirect:/admin/team/" + idTeam + "/update/player-error");
		} else {
			Team team = teamService.getTeam(idTeam);
			List<Player> players = team.getPlayers();
			players.add(player);
			team.setPlayers(players);
			teamService.saveTeam(team);

			return new ModelAndView("redirect:/admin/team/" + idTeam + "/update");
		}
	}

	@GetMapping("/admin/team/{id}/delete")
	public String deleteTeam(@PathVariable("id") int idTeam) {
		teamService.deleteTeam(idTeam);

		return "redirect:/admin/dashboard";
	}
}
