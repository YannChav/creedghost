package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.CompetitionSoloPlayer;
import fr.creedghost.website.repository.CompetitionSoloPlayerProxy;
import lombok.Data;

@Data
@Service
public class CompetitionSoloPlayerService {

	@Autowired
	private CompetitionSoloPlayerProxy competitionSoloPlayerProxy;

	public CompetitionSoloPlayer getCompetitionSoloPlayer(final int id) {
		return competitionSoloPlayerProxy.getCompetitionSoloPlayer(id);
	}

	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayersByPlayer(final int idPlayer) {
		return competitionSoloPlayerProxy.getCompetitionSoloPlayersByPlayer(idPlayer);
	}

	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayersByCompetition(final int idCompetition) {
		return competitionSoloPlayerProxy.getCompetitionSoloPlayersByCompetition(idCompetition);
	}

	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayers() {
		return competitionSoloPlayerProxy.getCompetitionSoloPlayers();
	}

	public CompetitionSoloPlayer saveCompetitionSoloPlayer(CompetitionSoloPlayer competitionSoloPlayer) {
		if (competitionSoloPlayer.getId() == null) {
			return competitionSoloPlayerProxy.createCompetitionSoloPlayer(competitionSoloPlayer);
		} else {
			return competitionSoloPlayerProxy.updateCompetitionSoloPlayer(competitionSoloPlayer);
		}
	}

	public void deleteCompetitionSoloPlayer(final int id) {
		competitionSoloPlayerProxy.deleteCompetitionSoloPlayer(id);
	}
}
