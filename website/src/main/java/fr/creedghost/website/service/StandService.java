package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.Stand;
import fr.creedghost.website.repository.StandProxy;
import lombok.Data;

@Data
@Service
public class StandService {

	@Autowired
	private StandProxy standProxy;

	public Stand getStand(final int id) {
		return standProxy.getStand(id);
	}

	public Iterable<Stand> getStandsByEvent(final int idEvent) {
		return standProxy.getStandsByEvent(idEvent);
	}

	public Iterable<Stand> getStands() {
		return standProxy.getStands();
	}

	public Stand saveStand(Stand stand) {
		if (stand.getId() == null) {
			return standProxy.createStand(stand);
		} else {
			return standProxy.updateStand(stand);
		}
	}

	public void deleteStand(final int id) {
		standProxy.deleteStand(id);
	}
}
