package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.Team;
import fr.creedghost.website.repository.TeamProxy;
import lombok.Data;

@Data
@Service
public class TeamService {

	@Autowired
	private TeamProxy teamProxy;

	public Team getTeam(final int id) {
		return teamProxy.getTeam(id);
	}

	public Team getTeamByName(final String name) {
		return teamProxy.getTeamByName(name);
	}

	public Iterable<Team> getTeams() {
		return teamProxy.getTeams();
	}

	public Team saveTeam(Team team) {
		if (team.getId() == null) {
			return teamProxy.createTeam(team);
		} else {
			return teamProxy.updateTeam(team);
		}
	}

	public void deleteTeam(final int id) {
		teamProxy.deleteTeam(id);
	}
}
