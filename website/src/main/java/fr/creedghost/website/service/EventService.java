package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.Event;
import fr.creedghost.website.repository.EventProxy;
import lombok.Data;

@Data
@Service
public class EventService {

	@Autowired
	private EventProxy eventProxy;

	public Event getEvent(final int id) {
		return eventProxy.getEvent(id);
	}

	public Iterable<Event> getEvents() {
		return eventProxy.getEvents();
	}

	public Event saveEvent(Event event) {
		if (event.getId() == null) {
			return eventProxy.createEvent(event);
		} else {
			return eventProxy.updateEvent(event);
		}
	}

	public void deleteEvent(final int id) {
		eventProxy.deleteEvent(id);
	}
}
