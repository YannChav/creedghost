package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.Conference;
import fr.creedghost.website.repository.ConferenceProxy;
import lombok.Data;

@Data
@Service
public class ConferenceService {

	@Autowired
	private ConferenceProxy conferenceProxy;

	public Conference getConference(final int id) {
		return conferenceProxy.getConference(id);
	}

	public Iterable<Conference> getConferencesByEvent(final int idEvent) {
		return conferenceProxy.getConferencesByEvent(idEvent);
	}

	public Iterable<Conference> getConferences() {
		return conferenceProxy.getConferences();
	}

	public Conference saveConference(Conference conference) {
		if (conference.getId() == null) {
			return conferenceProxy.createConference(conference);
		} else {
			return conferenceProxy.updateConference(conference);
		}
	}

	public void deleteConference(final int id) {
		conferenceProxy.deleteConference(id);
	}
}
