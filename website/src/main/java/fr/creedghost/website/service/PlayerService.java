package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.Player;
import fr.creedghost.website.repository.PlayerProxy;
import lombok.Data;

@Data
@Service
public class PlayerService {

	@Autowired
	private PlayerProxy playerProxy;

	public Player getPlayer(final int id) {
		return playerProxy.getPlayer(id);
	}

	public boolean verifiyPlayerExist(final String username, final String password) {
		Player player = playerProxy.getPlayerByUsername(username);

		if (player == null || MD5Service.getMd5(password).equals(player.getPassword()) == false) {
			return false;
		} else {
			return true;
		}
	}

	public Player GetPlayerByUsername(final String username) {
		Player player = playerProxy.getPlayerByUsername(username);

		return player;
	}

	public Iterable<Player> getPlayers() {
		return playerProxy.getPlayers();
	}

	public Player savePlayer(Player player) {
		if (player.getId() == null) {
			return playerProxy.createPlayer(player);
		} else {
			return playerProxy.updatePlayer(player);
		}
	}

	public void deletePlayer(final int id) {
		playerProxy.deletePlayer(id);
	}
}
