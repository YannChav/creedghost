package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.CompetitionTeamTeam;
import fr.creedghost.website.repository.CompetitionTeamTeamProxy;
import lombok.Data;

@Data
@Service
public class CompetitionTeamTeamService {

	@Autowired
	private CompetitionTeamTeamProxy competitionTeamTeamProxy;

	public CompetitionTeamTeam getCompetitionTeamTeam(final int id) {
		return competitionTeamTeamProxy.getCompetitionTeamTeam(id);
	}

	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeamsByTeam(final int idTeam) {
		return competitionTeamTeamProxy.getCompetitionTeamTeamsByTeam(idTeam);
	}

	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeamsByCompetition(final int idCompetition) {
		return competitionTeamTeamProxy.getCompetitionTeamTeamsByCompetition(idCompetition);
	}

	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeams() {
		return competitionTeamTeamProxy.getCompetitionTeamTeams();
	}

	public CompetitionTeamTeam saveCompetitionTeamTeam(CompetitionTeamTeam competitionTeamTeam) {
		if (competitionTeamTeam.getId() == null) {
			return competitionTeamTeamProxy.createCompetitionTeamTeam(competitionTeamTeam);
		} else {
			return competitionTeamTeamProxy.updateCompetitionTeamTeam(competitionTeamTeam);
		}
	}

	public void deleteCompetitionTeamTeam(final int id) {
		competitionTeamTeamProxy.deleteCompetitionTeamTeam(id);
	}
}
