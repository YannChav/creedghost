package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.CompetitionSolo;
import fr.creedghost.website.repository.CompetitionSoloProxy;
import lombok.Data;

@Data
@Service
public class CompetitionSoloService {

	@Autowired
	private CompetitionSoloProxy competitionSoloProxy;

	public CompetitionSolo getCompetitionSolo(final int id) {
		return competitionSoloProxy.getCompetitionSolo(id);
	}

	public Iterable<CompetitionSolo> getCompetitionsSoloByEvent(final int idEvent) {
		return competitionSoloProxy.getCompetitionsSoloByEvent(idEvent);
	}

	public Iterable<CompetitionSolo> getCompetitionsSolo() {
		return competitionSoloProxy.getCompetitionsSolo();
	}

	public CompetitionSolo saveCompetitionSolo(CompetitionSolo competitionSolo) {
		if (competitionSolo.getId() == null) {
			return competitionSoloProxy.createCompetitionSolo(competitionSolo);
		} else {
			return competitionSoloProxy.updateCompetitionSolo(competitionSolo);
		}
	}

	public void deleteCompetitionSolo(final int id) {
		competitionSoloProxy.deleteCompetitionSolo(id);
	}
}
