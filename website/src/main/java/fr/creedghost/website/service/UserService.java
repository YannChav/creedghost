package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.User;
import fr.creedghost.website.repository.UserProxy;
import lombok.Data;

@Data
@Service
public class UserService {

	@Autowired
	private UserProxy userProxy;

	public User getUser(final int id) {
		return userProxy.getUser(id);
	}

	public Iterable<User> getUsers() {
		return userProxy.getUsers();
	}

	public boolean verifiyUserExist(final String username, final String password) {
		User user = userProxy.getUserByUsername(username);

		if (user == null || MD5Service.getMd5(password).equals(user.getPassword()) == false) {
			return false;
		} else {
			return true;
		}
	}

	public User saveUser(User user) {
		if (user.getId() == null) {
			return userProxy.createUser(user);
		} else {
			return userProxy.updateUser(user);
		}
	}

	public void deleteUser(final int id) {
		userProxy.deleteUser(id);
	}
}
