package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.Activity;
import fr.creedghost.website.repository.ActivityProxy;
import lombok.Data;

@Data
@Service
public class ActivityService {

	@Autowired
	private ActivityProxy activityProxy;

	public Activity getActivity(final int id) {
		return activityProxy.getActivity(id);
	}

	public Iterable<Activity> getActivitiesByEvent(final int idEvent) {
		return activityProxy.getActivitiesByEvent(idEvent);
	}

	public Iterable<Activity> getActivities() {
		return activityProxy.getActivities();
	}

	public Activity saveActivity(Activity activity) {
		if (activity.getId() == null) {
			return activityProxy.createActivity(activity);
		} else {
			return activityProxy.updateActivity(activity);
		}
	}

	public void deleteActivity(final int id) {
		activityProxy.deleteActivity(id);
	}
}
