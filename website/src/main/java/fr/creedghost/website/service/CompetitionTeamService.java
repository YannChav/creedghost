package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.CompetitionTeam;
import fr.creedghost.website.repository.CompetitionTeamProxy;
import lombok.Data;

@Data
@Service
public class CompetitionTeamService {

	@Autowired
	private CompetitionTeamProxy competitionTeamProxy;

	public CompetitionTeam getCompetitionTeam(final int id) {
		return competitionTeamProxy.getCompetitionTeam(id);
	}

	public Iterable<CompetitionTeam> getCompetitionsTeamByEvent(final int idEvent) {
		return competitionTeamProxy.getCompetitionsTeamByEvent(idEvent);
	}

	public Iterable<CompetitionTeam> getCompetitionsTeam() {
		return competitionTeamProxy.getCompetitionsTeam();
	}

	public CompetitionTeam saveCompetitionTeam(CompetitionTeam competitionTeam) {
		if (competitionTeam.getId() == null) {
			return competitionTeamProxy.createCompetitionTeam(competitionTeam);
		} else {
			return competitionTeamProxy.updateCompetitionTeam(competitionTeam);
		}
	}

	public void deleteCompetitionTeam(final int id) {
		competitionTeamProxy.deleteCompetitionTeam(id);
	}
}
