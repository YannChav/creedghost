package fr.creedghost.website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.website.model.Speaker;
import fr.creedghost.website.repository.SpeakerProxy;
import lombok.Data;

@Data
@Service
public class SpeakerService {

	@Autowired
	private SpeakerProxy speakerProxy;

	public Speaker getSpeaker(final int id) {
		return speakerProxy.getSpeaker(id);
	}

	public Speaker getSpeakerByEmail(final String email) {
		return speakerProxy.getSpeakerByEmail(email);
	}

	public Iterable<Speaker> getSpeakers() {
		return speakerProxy.getSpeakers();
	}

	public Speaker saveSpeaker(Speaker speaker) {
		if (speaker.getId() == null) {
			return speakerProxy.createSpeaker(speaker);
		} else {
			return speakerProxy.updateSpeaker(speaker);
		}
	}

	public void deleteSpeaker(final int id) {
		speakerProxy.deleteSpeaker(id);
	}
}
