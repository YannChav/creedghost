package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.CompetitionSoloPlayer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CompetitionSoloPlayerProxy {
	@Autowired
	private CustomProperties props;

	public CompetitionSoloPlayer createCompetitionSoloPlayer(CompetitionSoloPlayer competitionSoloPlayer) {
		String baseApiUrl = props.getApiUrl();
		String createCompetitionSoloPlayerUrl = baseApiUrl + "/competition-solo-player";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<CompetitionSoloPlayer> request = new HttpEntity<CompetitionSoloPlayer>(competitionSoloPlayer);
		ResponseEntity<CompetitionSoloPlayer> response = restTemplate.exchange(createCompetitionSoloPlayerUrl,
				HttpMethod.POST, request, CompetitionSoloPlayer.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public CompetitionSoloPlayer getCompetitionSoloPlayer(int id) {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionSoloPlayerUrl = baseApiUrl + "/competition-solo-player/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<CompetitionSoloPlayer> response = restTemplate.exchange(getCompetitionSoloPlayerUrl,
				HttpMethod.GET, null, CompetitionSoloPlayer.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayersByPlayer(int idPlayer) {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionSoloPlayersUrl = baseApiUrl + "/competition-solo-players/player/" + idPlayer;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<CompetitionSoloPlayer>> response = restTemplate.exchange(getCompetitionSoloPlayersUrl,
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<CompetitionSoloPlayer>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayersByCompetition(int idCompetition) {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionSoloPlayersUrl = baseApiUrl + "/competition-solo-players/competition/" + idCompetition;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<CompetitionSoloPlayer>> response = restTemplate.exchange(getCompetitionSoloPlayersUrl,
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<CompetitionSoloPlayer>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayers() {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionSoloPlayersUrl = baseApiUrl + "/competition-solo-players";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<CompetitionSoloPlayer>> response = restTemplate.exchange(getCompetitionSoloPlayersUrl,
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<CompetitionSoloPlayer>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public CompetitionSoloPlayer updateCompetitionSoloPlayer(CompetitionSoloPlayer competitionSoloPlayer) {
		String baseApiUrl = props.getApiUrl();
		String updateCompetitionSoloPlayerUrl = baseApiUrl + "/competition-solo-player";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<CompetitionSoloPlayer> request = new HttpEntity<CompetitionSoloPlayer>(competitionSoloPlayer);
		ResponseEntity<CompetitionSoloPlayer> response = restTemplate.exchange(updateCompetitionSoloPlayerUrl,
				HttpMethod.PUT, request, CompetitionSoloPlayer.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteCompetitionSoloPlayer(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteCompetitionSoloPlayerUrl = baseApiUrl + "/competition-solo-player/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteCompetitionSoloPlayerUrl, HttpMethod.DELETE, null,
				Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
