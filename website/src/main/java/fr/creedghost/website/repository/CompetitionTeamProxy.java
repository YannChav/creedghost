package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.CompetitionTeam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CompetitionTeamProxy {

	@Autowired
	private CustomProperties props;

	public CompetitionTeam createCompetitionTeam(CompetitionTeam competitionTeam) {
		String baseApiUrl = props.getApiUrl();
		String createCompetitionTeamUrl = baseApiUrl + "/competition-team";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<CompetitionTeam> request = new HttpEntity<CompetitionTeam>(competitionTeam);
		ResponseEntity<CompetitionTeam> response = restTemplate.exchange(createCompetitionTeamUrl, HttpMethod.POST,
				request, CompetitionTeam.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public CompetitionTeam getCompetitionTeam(int id) {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionTeamUrl = baseApiUrl + "/competition-team/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<CompetitionTeam> response = restTemplate.exchange(getCompetitionTeamUrl, HttpMethod.GET, null,
				CompetitionTeam.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<CompetitionTeam> getCompetitionsTeamByEvent(int idEvent) {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionsTeamUrl = baseApiUrl + "/competitions-team/event/" + idEvent;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<CompetitionTeam>> response = restTemplate.exchange(getCompetitionsTeamUrl,
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<CompetitionTeam>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<CompetitionTeam> getCompetitionsTeam() {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionsTeamUrl = baseApiUrl + "/competitions-team";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<CompetitionTeam>> response = restTemplate.exchange(getCompetitionsTeamUrl,
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<CompetitionTeam>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public CompetitionTeam updateCompetitionTeam(CompetitionTeam competitionTeam) {
		String baseApiUrl = props.getApiUrl();
		String updateCompetitionTeamUrl = baseApiUrl + "/competition-team";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<CompetitionTeam> request = new HttpEntity<CompetitionTeam>(competitionTeam);
		ResponseEntity<CompetitionTeam> response = restTemplate.exchange(updateCompetitionTeamUrl, HttpMethod.PUT,
				request, CompetitionTeam.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteCompetitionTeam(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteCompetitionTeamUrl = baseApiUrl + "/competition-team/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteCompetitionTeamUrl, HttpMethod.DELETE, null,
				Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
