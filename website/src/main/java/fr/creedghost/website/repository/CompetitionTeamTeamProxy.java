package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.CompetitionTeamTeam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CompetitionTeamTeamProxy {

	@Autowired
	private CustomProperties props;

	public CompetitionTeamTeam createCompetitionTeamTeam(CompetitionTeamTeam competitionTeamTeam) {
		String baseApiUrl = props.getApiUrl();
		String createCompetitionTeamTeamUrl = baseApiUrl + "/competition-team-team";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<CompetitionTeamTeam> request = new HttpEntity<CompetitionTeamTeam>(competitionTeamTeam);
		ResponseEntity<CompetitionTeamTeam> response = restTemplate.exchange(createCompetitionTeamTeamUrl,
				HttpMethod.POST, request, CompetitionTeamTeam.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public CompetitionTeamTeam getCompetitionTeamTeam(int id) {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionTeamTeamUrl = baseApiUrl + "/competition-team-team/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<CompetitionTeamTeam> response = restTemplate.exchange(getCompetitionTeamTeamUrl, HttpMethod.GET,
				null, CompetitionTeamTeam.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeamsByTeam(int idTeam) {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionTeamTeamsUrl = baseApiUrl + "/competition-team-teams/team/" + idTeam;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<CompetitionTeamTeam>> response = restTemplate.exchange(getCompetitionTeamTeamsUrl,
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<CompetitionTeamTeam>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeamsByCompetition(int idCompetition) {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionTeamTeamsUrl = baseApiUrl + "/competition-team-teams/competition/" + idCompetition;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<CompetitionTeamTeam>> response = restTemplate.exchange(getCompetitionTeamTeamsUrl,
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<CompetitionTeamTeam>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeams() {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionTeamTeamsUrl = baseApiUrl + "/competition-team-teams";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<CompetitionTeamTeam>> response = restTemplate.exchange(getCompetitionTeamTeamsUrl,
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<CompetitionTeamTeam>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public CompetitionTeamTeam updateCompetitionTeamTeam(CompetitionTeamTeam competitionTeamTeam) {
		String baseApiUrl = props.getApiUrl();
		String updateCompetitionTeamTeamUrl = baseApiUrl + "/competition-team-team";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<CompetitionTeamTeam> request = new HttpEntity<CompetitionTeamTeam>(competitionTeamTeam);
		ResponseEntity<CompetitionTeamTeam> response = restTemplate.exchange(updateCompetitionTeamTeamUrl,
				HttpMethod.PUT, request, CompetitionTeamTeam.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteCompetitionTeamTeam(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteCompetitionTeamTeamUrl = baseApiUrl + "/competition-team-team/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteCompetitionTeamTeamUrl, HttpMethod.DELETE, null,
				Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
