package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.Activity;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ActivityProxy {

	@Autowired
	private CustomProperties props;

	public Activity createActivity(Activity activity) {
		String baseApiUrl = props.getApiUrl();
		String createActivityUrl = baseApiUrl + "/activity";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Activity> request = new HttpEntity<Activity>(activity);
		ResponseEntity<Activity> response = restTemplate.exchange(createActivityUrl, HttpMethod.POST, request,
				Activity.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Activity getActivity(int id) {
		String baseApiUrl = props.getApiUrl();
		String getActivityUrl = baseApiUrl + "/activity/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Activity> response = restTemplate.exchange(getActivityUrl, HttpMethod.GET, null, Activity.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<Activity> getActivitiesByEvent(int idEvent) {
		String baseApiUrl = props.getApiUrl();
		String getActivitiesUrl = baseApiUrl + "/activities/event/" + idEvent;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Activity>> response = restTemplate.exchange(getActivitiesUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Activity>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<Activity> getActivities() {
		String baseApiUrl = props.getApiUrl();
		String getActivitiesUrl = baseApiUrl + "/activities";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Activity>> response = restTemplate.exchange(getActivitiesUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Activity>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Activity updateActivity(Activity activity) {
		String baseApiUrl = props.getApiUrl();
		String updateActivityUrl = baseApiUrl + "/activity";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Activity> request = new HttpEntity<Activity>(activity);
		ResponseEntity<Activity> response = restTemplate.exchange(updateActivityUrl, HttpMethod.PUT, request,
				Activity.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteActivity(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteActivityUrl = baseApiUrl + "/activity/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteActivityUrl, HttpMethod.DELETE, null, Void.class);

		log.debug(response.getStatusCode().toString());
	}

}
