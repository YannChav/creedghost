package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.Team;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TeamProxy {

	@Autowired
	private CustomProperties props;

	public Team createTeam(Team team) {
		String baseApiUrl = props.getApiUrl();
		String createTeamUrl = baseApiUrl + "/team";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Team> request = new HttpEntity<Team>(team);
		ResponseEntity<Team> response = restTemplate.exchange(createTeamUrl, HttpMethod.POST, request, Team.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Team getTeam(int id) {
		String baseApiUrl = props.getApiUrl();
		String getTeamUrl = baseApiUrl + "/team/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Team> response = restTemplate.exchange(getTeamUrl, HttpMethod.GET, null, Team.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Team getTeamByName(String name) {
		String baseApiUrl = props.getApiUrl();
		String getTeamUrl = baseApiUrl + "/team/name/" + name;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Team> response = restTemplate.exchange(getTeamUrl, HttpMethod.GET, null, Team.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<Team> getTeams() {
		String baseApiUrl = props.getApiUrl();
		String getTeamsUrl = baseApiUrl + "/teams";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Team>> response = restTemplate.exchange(getTeamsUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Team>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Team updateTeam(Team team) {
		String baseApiUrl = props.getApiUrl();
		String updateTeamUrl = baseApiUrl + "/team";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Team> request = new HttpEntity<Team>(team);
		ResponseEntity<Team> response = restTemplate.exchange(updateTeamUrl, HttpMethod.PUT, request, Team.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteTeam(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteTeamUrl = baseApiUrl + "/team/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteTeamUrl, HttpMethod.DELETE, null, Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
