package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.Conference;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ConferenceProxy {

	@Autowired
	private CustomProperties props;

	public Conference createConference(Conference conference) {
		String baseApiUrl = props.getApiUrl();
		String createConferenceUrl = baseApiUrl + "/conference";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Conference> request = new HttpEntity<Conference>(conference);
		ResponseEntity<Conference> response = restTemplate.exchange(createConferenceUrl, HttpMethod.POST, request,
				Conference.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Conference getConference(int id) {
		String baseApiUrl = props.getApiUrl();
		String getConferenceUrl = baseApiUrl + "/conference/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Conference> response = restTemplate.exchange(getConferenceUrl, HttpMethod.GET, null,
				Conference.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<Conference> getConferencesByEvent(int idEvent) {
		String baseApiUrl = props.getApiUrl();
		String getConferencesUrl = baseApiUrl + "/conferences/event/" + idEvent;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Conference>> response = restTemplate.exchange(getConferencesUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Conference>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<Conference> getConferences() {
		String baseApiUrl = props.getApiUrl();
		String getConferencesUrl = baseApiUrl + "/conferences";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Conference>> response = restTemplate.exchange(getConferencesUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Conference>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Conference updateConference(Conference conference) {
		String baseApiUrl = props.getApiUrl();
		String updateConferenceUrl = baseApiUrl + "/conference";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Conference> request = new HttpEntity<Conference>(conference);
		ResponseEntity<Conference> response = restTemplate.exchange(updateConferenceUrl, HttpMethod.PUT, request,
				Conference.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteConference(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteConferenceUrl = baseApiUrl + "/conference/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteConferenceUrl, HttpMethod.DELETE, null, Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
