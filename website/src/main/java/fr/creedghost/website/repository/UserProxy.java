package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserProxy {

	@Autowired
	private CustomProperties props;

	public User createUser(User user) {
		String baseApiUrl = props.getApiUrl();
		String createUserUrl = baseApiUrl + "/user";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<User> request = new HttpEntity<User>(user);
		ResponseEntity<User> response = restTemplate.exchange(createUserUrl, HttpMethod.POST, request, User.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public User getUser(int id) {
		String baseApiUrl = props.getApiUrl();
		String getUserUrl = baseApiUrl + "/user/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<User> response = restTemplate.exchange(getUserUrl, HttpMethod.GET, null, User.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public User getUserByUsername(String username) {
		String baseApiUrl = props.getApiUrl();
		String getUserUrl = baseApiUrl + "/user/username/" + username;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<User> response = restTemplate.exchange(getUserUrl, HttpMethod.GET, null, User.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<User> getUsers() {
		String baseApiUrl = props.getApiUrl();
		String getUsersUrl = baseApiUrl + "/users";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<User>> response = restTemplate.exchange(getUsersUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<User>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public User updateUser(User user) {
		String baseApiUrl = props.getApiUrl();
		String updateUserUrl = baseApiUrl + "/user";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<User> request = new HttpEntity<User>(user);
		ResponseEntity<User> response = restTemplate.exchange(updateUserUrl, HttpMethod.PUT, request, User.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteUser(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteUserUrl = baseApiUrl + "/user/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteUserUrl, HttpMethod.DELETE, null, Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
