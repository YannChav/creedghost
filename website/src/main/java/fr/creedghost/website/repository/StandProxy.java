package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.Stand;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class StandProxy {

	@Autowired
	private CustomProperties props;

	public Stand createStand(Stand stand) {
		String baseApiUrl = props.getApiUrl();
		String createStandUrl = baseApiUrl + "/stand";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Stand> request = new HttpEntity<Stand>(stand);
		ResponseEntity<Stand> response = restTemplate.exchange(createStandUrl, HttpMethod.POST, request, Stand.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Stand getStand(int id) {
		String baseApiUrl = props.getApiUrl();
		String getStandUrl = baseApiUrl + "/stand/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Stand> response = restTemplate.exchange(getStandUrl, HttpMethod.GET, null, Stand.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<Stand> getStandsByEvent(int idEvent) {
		String baseApiUrl = props.getApiUrl();
		String getStandsUrl = baseApiUrl + "/stands/event/" + idEvent;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Stand>> response = restTemplate.exchange(getStandsUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Stand>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<Stand> getStands() {
		String baseApiUrl = props.getApiUrl();
		String getStandsUrl = baseApiUrl + "/stands";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Stand>> response = restTemplate.exchange(getStandsUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Stand>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Stand updateStand(Stand stand) {
		String baseApiUrl = props.getApiUrl();
		String updateStandUrl = baseApiUrl + "/stand";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Stand> request = new HttpEntity<Stand>(stand);
		ResponseEntity<Stand> response = restTemplate.exchange(updateStandUrl, HttpMethod.PUT, request, Stand.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteStand(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteStandUrl = baseApiUrl + "/stand/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteStandUrl, HttpMethod.DELETE, null, Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
