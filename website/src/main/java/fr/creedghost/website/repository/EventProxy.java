package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.Event;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class EventProxy {

	@Autowired
	private CustomProperties props;

	public Event createEvent(Event event) {
		String baseApiUrl = props.getApiUrl();
		String createEventUrl = baseApiUrl + "/event";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Event> request = new HttpEntity<Event>(event);
		ResponseEntity<Event> response = restTemplate.exchange(createEventUrl, HttpMethod.POST, request, Event.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Event getEvent(int id) {
		String baseApiUrl = props.getApiUrl();
		String getEventUrl = baseApiUrl + "/event/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Event> response = restTemplate.exchange(getEventUrl, HttpMethod.GET, null, Event.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<Event> getEvents() {
		String baseApiUrl = props.getApiUrl();
		String getEventsUrl = baseApiUrl + "/events";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Event>> response = restTemplate.exchange(getEventsUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Event>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Event updateEvent(Event event) {
		String baseApiUrl = props.getApiUrl();
		String updateEventUrl = baseApiUrl + "/event";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Event> request = new HttpEntity<Event>(event);
		ResponseEntity<Event> response = restTemplate.exchange(updateEventUrl, HttpMethod.PUT, request, Event.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteEvent(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteEventUrl = baseApiUrl + "/event/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteEventUrl, HttpMethod.DELETE, null, Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
