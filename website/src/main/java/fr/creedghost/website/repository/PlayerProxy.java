package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.Player;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PlayerProxy {

	@Autowired
	private CustomProperties props;

	public Player createPlayer(Player player) {
		String baseApiUrl = props.getApiUrl();
		String createPlayerUrl = baseApiUrl + "/player";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Player> request = new HttpEntity<Player>(player);
		ResponseEntity<Player> response = restTemplate.exchange(createPlayerUrl, HttpMethod.POST, request,
				Player.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Player getPlayer(int id) {
		String baseApiUrl = props.getApiUrl();
		String getPlayerUrl = baseApiUrl + "/player/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Player> response = restTemplate.exchange(getPlayerUrl, HttpMethod.GET, null, Player.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Player getPlayerByUsername(String username) {
		String baseApiUrl = props.getApiUrl();
		String getPlayerUrl = baseApiUrl + "/player/username/" + username;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Player> response = restTemplate.exchange(getPlayerUrl, HttpMethod.GET, null, Player.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<Player> getPlayers() {
		String baseApiUrl = props.getApiUrl();
		String getPlayersUrl = baseApiUrl + "/players";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Player>> response = restTemplate.exchange(getPlayersUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Player>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Player updatePlayer(Player player) {
		String baseApiUrl = props.getApiUrl();
		String updatePlayerUrl = baseApiUrl + "/player";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Player> request = new HttpEntity<Player>(player);
		ResponseEntity<Player> response = restTemplate.exchange(updatePlayerUrl, HttpMethod.PUT, request, Player.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deletePlayer(int id) {
		String baseApiUrl = props.getApiUrl();
		String deletePlayerUrl = baseApiUrl + "/player/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deletePlayerUrl, HttpMethod.DELETE, null, Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
