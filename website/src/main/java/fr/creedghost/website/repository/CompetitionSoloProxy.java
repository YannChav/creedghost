package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.CompetitionSolo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CompetitionSoloProxy {

	@Autowired
	private CustomProperties props;

	public CompetitionSolo createCompetitionSolo(CompetitionSolo competitionSolo) {
		String baseApiUrl = props.getApiUrl();
		String createCompetitionSoloUrl = baseApiUrl + "/competition-solo";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<CompetitionSolo> request = new HttpEntity<CompetitionSolo>(competitionSolo);
		ResponseEntity<CompetitionSolo> response = restTemplate.exchange(createCompetitionSoloUrl, HttpMethod.POST,
				request, CompetitionSolo.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public CompetitionSolo getCompetitionSolo(int id) {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionSoloUrl = baseApiUrl + "/competition-solo/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<CompetitionSolo> response = restTemplate.exchange(getCompetitionSoloUrl, HttpMethod.GET, null,
				CompetitionSolo.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<CompetitionSolo> getCompetitionsSoloByEvent(int idEvent) {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionsSoloUrl = baseApiUrl + "/competitions-solo/event/" + idEvent;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<CompetitionSolo>> response = restTemplate.exchange(getCompetitionsSoloUrl,
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<CompetitionSolo>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<CompetitionSolo> getCompetitionsSolo() {
		String baseApiUrl = props.getApiUrl();
		String getCompetitionsSoloUrl = baseApiUrl + "/competitions-solo";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<CompetitionSolo>> response = restTemplate.exchange(getCompetitionsSoloUrl,
				HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<CompetitionSolo>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public CompetitionSolo updateCompetitionSolo(CompetitionSolo competitionSolo) {
		String baseApiUrl = props.getApiUrl();
		String updateCompetitionSoloUrl = baseApiUrl + "/competition-solo";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<CompetitionSolo> request = new HttpEntity<CompetitionSolo>(competitionSolo);
		ResponseEntity<CompetitionSolo> response = restTemplate.exchange(updateCompetitionSoloUrl, HttpMethod.PUT,
				request, CompetitionSolo.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteCompetitionSolo(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteCompetitionSoloUrl = baseApiUrl + "/competition-solo/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteCompetitionSoloUrl, HttpMethod.DELETE, null,
				Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
