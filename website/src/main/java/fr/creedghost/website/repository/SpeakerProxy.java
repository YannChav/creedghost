package fr.creedghost.website.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.creedghost.website.CustomProperties;
import fr.creedghost.website.model.Speaker;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SpeakerProxy {

	@Autowired
	private CustomProperties props;

	public Speaker createSpeaker(Speaker speaker) {
		String baseApiUrl = props.getApiUrl();
		String createSpeakerUrl = baseApiUrl + "/speaker";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Speaker> request = new HttpEntity<Speaker>(speaker);
		ResponseEntity<Speaker> response = restTemplate.exchange(createSpeakerUrl, HttpMethod.POST, request,
				Speaker.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Speaker getSpeaker(int id) {
		String baseApiUrl = props.getApiUrl();
		String getSpeakerUrl = baseApiUrl + "/speaker/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Speaker> response = restTemplate.exchange(getSpeakerUrl, HttpMethod.GET, null, Speaker.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Speaker getSpeakerByEmail(String email) {
		String baseApiUrl = props.getApiUrl();
		String getSpeakerUrl = baseApiUrl + "/speaker/email/" + email;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Speaker> response = restTemplate.exchange(getSpeakerUrl, HttpMethod.GET, null, Speaker.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Iterable<Speaker> getSpeakers() {
		String baseApiUrl = props.getApiUrl();
		String getSpeakersUrl = baseApiUrl + "/speakers";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Speaker>> response = restTemplate.exchange(getSpeakersUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<Iterable<Speaker>>() {
				});

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public Speaker updateSpeaker(Speaker speaker) {
		String baseApiUrl = props.getApiUrl();
		String updateSpeakerUrl = baseApiUrl + "/speaker";

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Speaker> request = new HttpEntity<Speaker>(speaker);
		ResponseEntity<Speaker> response = restTemplate.exchange(updateSpeakerUrl, HttpMethod.PUT, request,
				Speaker.class);

		log.debug(response.getStatusCode().toString());

		return response.getBody();
	}

	public void deleteSpeaker(int id) {
		String baseApiUrl = props.getApiUrl();
		String deleteSpeakerUrl = baseApiUrl + "/speaker/" + id;

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.exchange(deleteSpeakerUrl, HttpMethod.DELETE, null, Void.class);

		log.debug(response.getStatusCode().toString());
	}
}
