package fr.creedghost.website;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "fr.creedghost.website")
public class CustomProperties {

	private String apiUrl;

}
