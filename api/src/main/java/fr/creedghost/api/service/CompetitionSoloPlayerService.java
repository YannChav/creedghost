package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.CompetitionSolo;
import fr.creedghost.api.model.CompetitionSoloPlayer;
import fr.creedghost.api.model.Player;
import fr.creedghost.api.repository.CompetitionSoloPlayerRepository;
import lombok.Data;

@Data
@Service
public class CompetitionSoloPlayerService {
	@Autowired
	private CompetitionSoloPlayerRepository competitionSoloPlayerRepository;

	public Optional<CompetitionSoloPlayer> getCompetitionSoloPlayer(final Long id) {
		return competitionSoloPlayerRepository.findById(id);
	}

	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayersByPlayer(final Player player) {
		return competitionSoloPlayerRepository.findByPlayer(player);
	}

	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayersByCompetition(
			final CompetitionSolo competitionSolo) {
		return competitionSoloPlayerRepository.findByCompetition(competitionSolo);
	}

	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayers() {
		return competitionSoloPlayerRepository.findAll();
	}

	public CompetitionSoloPlayer saveCompetitionSoloPlayer(CompetitionSoloPlayer competitionSoloPlayer) {
		return competitionSoloPlayerRepository.save(competitionSoloPlayer);
	}

	public void deleteCompetitionSoloPlayer(final Long id) {
		competitionSoloPlayerRepository.deleteById(id);
	}
}
