package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.Player;
import fr.creedghost.api.repository.PlayerRepository;
import lombok.Data;

@Data
@Service
public class PlayerService {
	@Autowired
	private PlayerRepository playerRepository;

	public Optional<Player> getPlayer(final Long id) {
		return playerRepository.findById(id);
	}

	public Optional<Player> getPlayerByUsername(final String username) {
		return playerRepository.findByUsername(username);
	}

	public Iterable<Player> getPlayers() {
		return playerRepository.findAll();
	}

	public Player savePlayer(Player player) {
		return playerRepository.save(player);
	}

	public void deletePlayer(final Long id) {
		playerRepository.deleteById(id);
	}
}
