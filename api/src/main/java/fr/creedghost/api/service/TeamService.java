package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.Team;
import fr.creedghost.api.repository.TeamRepository;
import lombok.Data;

@Data
@Service
public class TeamService {
	@Autowired
	private TeamRepository teamRepository;

	public Optional<Team> getTeam(final Long id) {
		return teamRepository.findById(id);
	}

	public Optional<Team> getTeamByName(final String name) {
		return teamRepository.findByName(name);
	}

	public Iterable<Team> getTeamsByPlayerSQL(final int idPlayer) {
		return teamRepository.findByPlayerSQL(idPlayer);
	}

	public Iterable<Team> getTeams() {
		return teamRepository.findAll();
	}

	public Team saveTeam(Team team) {
		return teamRepository.save(team);
	}

	public void deleteTeam(final Long id) {
		teamRepository.deleteById(id);
	}
}
