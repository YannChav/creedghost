package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.CompetitionTeam;
import fr.creedghost.api.model.CompetitionTeamTeam;
import fr.creedghost.api.model.Team;
import fr.creedghost.api.repository.CompetitionTeamTeamRepository;
import lombok.Data;

@Data
@Service
public class CompetitionTeamTeamService {
	@Autowired
	private CompetitionTeamTeamRepository competitionTeamTeamRepository;

	public Optional<CompetitionTeamTeam> getCompetitionTeamTeam(final Long id) {
		return competitionTeamTeamRepository.findById(id);
	}

	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeamsByTeam(final Team team) {
		return competitionTeamTeamRepository.findByTeam(team);
	}

	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeamsByCompetition(final CompetitionTeam competitionTeam) {
		return competitionTeamTeamRepository.findByCompetition(competitionTeam);
	}

	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeams() {
		return competitionTeamTeamRepository.findAll();
	}

	public CompetitionTeamTeam saveCompetitionTeamTeam(CompetitionTeamTeam competitionTeamTeam) {
		return competitionTeamTeamRepository.save(competitionTeamTeam);
	}

	public void deleteCompetitionTeamTeam(final Long id) {
		competitionTeamTeamRepository.deleteById(id);
	}
}
