package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.Event;
import fr.creedghost.api.model.Stand;
import fr.creedghost.api.repository.StandRepository;
import lombok.Data;

@Data
@Service
public class StandService {
	@Autowired
	private StandRepository standRepository;

	public Optional<Stand> getStand(final Long id) {
		return standRepository.findById(id);
	}

	public Iterable<Stand> getStandsByEvent(final Event event) {
		return standRepository.findByEvent(event);
	}

	public Iterable<Stand> getStands() {
		return standRepository.findAll();
	}

	public Stand saveStand(Stand stand) {
		return standRepository.save(stand);
	}

	public void deleteStand(final Long id) {
		standRepository.deleteById(id);
	}
}
