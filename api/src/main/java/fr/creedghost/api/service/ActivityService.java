package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.Activity;
import fr.creedghost.api.model.Event;
import fr.creedghost.api.repository.ActivityRepository;
import lombok.Data;

@Data
@Service
public class ActivityService {

	@Autowired
	private ActivityRepository activityRepository;

	public Optional<Activity> getActivity(final Long id) {
		return activityRepository.findById(id);
	}

	public Iterable<Activity> getActivitiesByEvent(final Event event) {
		return activityRepository.findByEvent(event);
	}

	public Iterable<Activity> getActivities() {
		return activityRepository.findAll();
	}

	public Activity saveActivity(Activity activity) {
		return activityRepository.save(activity);
	}

	public void deleteActivity(final Long id) {
		activityRepository.deleteById(id);
	}
}
