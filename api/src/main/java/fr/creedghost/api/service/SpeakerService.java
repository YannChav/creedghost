package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.Speaker;
import fr.creedghost.api.repository.SpeakerRepository;
import lombok.Data;

@Data
@Service
public class SpeakerService {
	@Autowired
	private SpeakerRepository speakerRepository;

	public Optional<Speaker> getSpeaker(final Long id) {
		return speakerRepository.findById(id);
	}

	public Optional<Speaker> getSpeakerByEmail(final String email) {
		return speakerRepository.findByEmail(email);
	}

	public Iterable<Speaker> getSpeakers() {
		return speakerRepository.findAll();
	}

	public Speaker saveSpeaker(Speaker speaker) {
		return speakerRepository.save(speaker);
	}

	public void deleteSpeaker(final Long id) {
		speakerRepository.deleteById(id);
	}
}
