package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.Event;
import fr.creedghost.api.repository.EventRepository;
import lombok.Data;

@Data
@Service
public class EventService {
	@Autowired
	private EventRepository eventRepository;

	public Optional<Event> getEvent(final Long id) {
		return eventRepository.findById(id);
	}

	public Iterable<Event> getEvents() {
		return eventRepository.findAll();
	}

	public Event saveEvent(Event event) {
		return eventRepository.save(event);
	}

	public void deleteEvent(final Long id) {
		eventRepository.deleteById(id);
	}
}
