package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.CompetitionTeam;
import fr.creedghost.api.model.Event;
import fr.creedghost.api.repository.CompetitionTeamRepository;
import lombok.Data;

@Data
@Service
public class CompetitionTeamService {
	@Autowired
	CompetitionTeamRepository competitionTeamRepository;

	public Optional<CompetitionTeam> getCompetitionTeam(final Long id) {
		return competitionTeamRepository.findById(id);
	}

	public Iterable<CompetitionTeam> getCompetitionsTeamByEvent(final Event event) {
		return competitionTeamRepository.findByEvent(event);
	}

	public Iterable<CompetitionTeam> getCompetitionsTeam() {
		return competitionTeamRepository.findAll();
	}

	public CompetitionTeam saveCompetitionTeam(CompetitionTeam competitionTeam) {
		return competitionTeamRepository.save(competitionTeam);
	}

	public void DeleteCompetitionTeam(final Long id) {
		competitionTeamRepository.deleteById(id);
	}
}
