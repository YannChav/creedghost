package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.User;
import fr.creedghost.api.repository.UserRepository;
import lombok.Data;

@Data
@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	public Optional<User> getUser(final Long id) {
		return userRepository.findById(id);
	}

	public Optional<User> getUserByUsername(final String username) {
		return userRepository.findByUsername(username);
	}

	public Iterable<User> getUsers() {
		return userRepository.findAll();
	}

	public User saveUser(User user) {
		return userRepository.save(user);
	}

	public void deleteUser(final Long id) {
		userRepository.deleteById(id);
	}
}
