package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.Conference;
import fr.creedghost.api.model.Event;
import fr.creedghost.api.repository.ConferenceRepository;
import lombok.Data;

@Data
@Service
public class ConferenceService {
	@Autowired
	private ConferenceRepository conferenceRepository;

	public Optional<Conference> getConference(final Long id) {
		return conferenceRepository.findById(id);
	}

	public Iterable<Conference> getConferencesByEvent(final Event event) {
		return conferenceRepository.findByEvent(event);
	}

	public Iterable<Conference> getConferences() {
		return conferenceRepository.findAll();
	}

	public Conference saveConference(Conference conference) {
		return conferenceRepository.save(conference);
	}

	public void deleteConference(final Long id) {
		conferenceRepository.deleteById(id);
	}
}
