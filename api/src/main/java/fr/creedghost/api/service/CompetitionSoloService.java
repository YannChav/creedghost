package fr.creedghost.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.creedghost.api.model.CompetitionSolo;
import fr.creedghost.api.model.Event;
import fr.creedghost.api.repository.CompetitionSoloRepository;
import lombok.Data;

@Data
@Service
public class CompetitionSoloService {
	@Autowired
	private CompetitionSoloRepository competitionSoloRepository;

	public Optional<CompetitionSolo> getCompetitionSolo(final Long id) {
		return competitionSoloRepository.findById(id);
	}

	public Iterable<CompetitionSolo> getCompetitionsSoloByEvent(final Event event) {
		return competitionSoloRepository.findByEvent(event);
	}

	public Iterable<CompetitionSolo> getCompetitionsSolo() {
		return competitionSoloRepository.findAll();
	}

	public CompetitionSolo saveCompetitionSolo(CompetitionSolo competitionSolo) {
		return competitionSoloRepository.save(competitionSolo);
	}

	public void deleteCompetitionSolo(final Long id) {
		competitionSoloRepository.deleteById(id);
	}
}
