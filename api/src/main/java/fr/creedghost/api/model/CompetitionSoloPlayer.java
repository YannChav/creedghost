package fr.creedghost.api.model;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "competition_solo_player")
public class CompetitionSoloPlayer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "registration_date")
	private Date registrationDate;

	@ManyToOne
	@JoinColumn(name = "player_id")
	private Player player;

	private int ranking;

	@ManyToOne
	@JoinColumn(name = "competition_id")
	private CompetitionSolo competition;
}
