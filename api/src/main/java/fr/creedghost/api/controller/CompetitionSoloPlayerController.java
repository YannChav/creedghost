package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.CompetitionSolo;
import fr.creedghost.api.model.CompetitionSoloPlayer;
import fr.creedghost.api.model.Player;
import fr.creedghost.api.service.CompetitionSoloPlayerService;

@RestController
public class CompetitionSoloPlayerController {
	@Autowired
	private CompetitionSoloPlayerService competitionSoloPlayerService;

	@PostMapping("/competition-solo-player")
	public CompetitionSoloPlayer createCompetitionSoloPlayer(@RequestBody CompetitionSoloPlayer competitionSoloPlayer) {
		return competitionSoloPlayerService.saveCompetitionSoloPlayer(competitionSoloPlayer);
	}

	@GetMapping("/competition-solo-player/{id}")
	public CompetitionSoloPlayer getCompetitionSoloPlayer(@PathVariable("id") final Long id) {
		Optional<CompetitionSoloPlayer> competitionSoloPlayer = competitionSoloPlayerService
				.getCompetitionSoloPlayer(id);
		if (competitionSoloPlayer.isPresent()) {
			return competitionSoloPlayer.get();
		} else {
			return null;
		}
	}

	@GetMapping("/competition-solo-players/player/{id}")
	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayersByPlayer(@PathVariable("id") final Long idPlayer) {
		Player player = new Player();
		player.setId(idPlayer);

		return competitionSoloPlayerService.getCompetitionSoloPlayersByPlayer(player);
	}

	@GetMapping("/competition-solo-players/competition/{id}")
	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayersByCompetition(
			@PathVariable("id") final Long idCompetition) {
		CompetitionSolo competitionSolo = new CompetitionSolo();
		competitionSolo.setId(idCompetition);

		return competitionSoloPlayerService.getCompetitionSoloPlayersByCompetition(competitionSolo);
	}

	@GetMapping("/competition-solo-players")
	public Iterable<CompetitionSoloPlayer> getCompetitionSoloPlayers() {
		return competitionSoloPlayerService.getCompetitionSoloPlayers();
	}

	@PutMapping("/competition-solo-player")
	public CompetitionSoloPlayer updateCompetitionSoloPlayer(@RequestBody CompetitionSoloPlayer competitionSoloPlayer) {
		Optional<CompetitionSoloPlayer> csp = competitionSoloPlayerService
				.getCompetitionSoloPlayer(competitionSoloPlayer.getId());
		if (csp.isPresent()) {
			return competitionSoloPlayerService.saveCompetitionSoloPlayer(competitionSoloPlayer);
		} else {
			return null;
		}
	}

	@DeleteMapping("/competition-solo-player/{id}")
	public void deleteCompetitionSoloPlayer(@PathVariable("id") final Long id) {
		competitionSoloPlayerService.deleteCompetitionSoloPlayer(id);
	}
}
