package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.Event;
import fr.creedghost.api.model.Stand;
import fr.creedghost.api.service.StandService;

@RestController
public class StandController {
	@Autowired
	private StandService standService;

	@PostMapping("/stand")
	public Stand createStand(@RequestBody Stand stand) {
		return standService.saveStand(stand);
	}

	@GetMapping("/stand/{id}")
	public Stand getStand(@PathVariable("id") final Long id) {
		Optional<Stand> s = standService.getStand(id);
		if (s.isPresent()) {
			return s.get();
		} else {
			return null;
		}
	}

	@GetMapping("/stands/event/{id}")
	public Iterable<Stand> getStandsByEvent(@PathVariable("id") final Long idEvent) {
		Event event = new Event();
		event.setId(idEvent);

		return standService.getStandsByEvent(event);
	}

	@GetMapping("/stands")
	public Iterable<Stand> getStands() {
		return standService.getStands();
	}

	@PutMapping("/stand")
	public Stand updateStand(@RequestBody Stand stand) {
		Optional<Stand> s = standService.getStand(stand.getId());
		if (s.isPresent()) {
			return standService.saveStand(stand);
		} else {
			return null;
		}
	}

	@DeleteMapping("/stand/{id}")
	public void deleteStand(@PathVariable("id") final Long id) {
		standService.deleteStand(id);
	}
}
