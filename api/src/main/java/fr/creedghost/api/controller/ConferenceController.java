package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.Conference;
import fr.creedghost.api.model.Event;
import fr.creedghost.api.service.ConferenceService;

@RestController
public class ConferenceController {
	@Autowired
	private ConferenceService conferenceService;

	@PostMapping("/conference")
	public Conference createConference(@RequestBody Conference conference) {
		return conferenceService.saveConference(conference);
	}

	@GetMapping("/conference/{id}")
	public Conference getConference(@PathVariable("id") final Long id) {
		Optional<Conference> c = conferenceService.getConference(id);
		if (c.isPresent()) {
			return c.get();
		} else {
			return null;
		}
	}

	@GetMapping("/conferences/event/{id}")
	public Iterable<Conference> getConferencesByEvent(@PathVariable("id") final Long idEvent) {
		Event event = new Event();
		event.setId(idEvent);

		return conferenceService.getConferencesByEvent(event);
	}

	@GetMapping("/conferences")
	public Iterable<Conference> getConferences() {
		return conferenceService.getConferences();
	}

	@PutMapping("/conference")
	public Conference updateConference(@RequestBody Conference conference) {
		Optional<Conference> c = conferenceService.getConference(conference.getId());
		if (c.isPresent()) {
			return conferenceService.saveConference(conference);
		} else {
			return null;
		}
	}

	@DeleteMapping("/conference/{id}")
	public void deleteConference(@PathVariable("id") final Long id) {
		conferenceService.deleteConference(id);
	}
}
