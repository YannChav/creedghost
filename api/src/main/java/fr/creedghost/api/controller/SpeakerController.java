package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.Speaker;
import fr.creedghost.api.service.SpeakerService;

@RestController
public class SpeakerController {
	@Autowired
	private SpeakerService speakerService;

	@PostMapping("/speaker")
	public Speaker createSpeaker(@RequestBody Speaker speaker) {
		return speakerService.saveSpeaker(speaker);
	}

	@GetMapping("/speaker/{id}")
	public Speaker getSpeaker(@PathVariable("id") final Long id) {
		Optional<Speaker> s = speakerService.getSpeaker(id);
		if (s.isPresent()) {
			return s.get();
		} else {
			return null;
		}
	}

	@GetMapping("/speaker/email/{email}")
	public Speaker getSpeakerByEmail(@PathVariable("email") final String email) {
		Optional<Speaker> s = speakerService.getSpeakerByEmail(email);
		if (s.isPresent()) {
			return s.get();
		} else {
			return null;
		}
	}

	@GetMapping("/speakers")
	public Iterable<Speaker> getSpeakers() {
		return speakerService.getSpeakers();
	}

	@PutMapping("/speaker")
	public Speaker updateSpeaker(@RequestBody Speaker speaker) {
		Optional<Speaker> s = speakerService.getSpeaker(speaker.getId());
		if (s.isPresent()) {
			return speakerService.saveSpeaker(speaker);
		} else {
			return null;
		}
	}

	@DeleteMapping("/speaker/{id}")
	public void deleteSpeaker(@PathVariable("id") final Long id) {
		speakerService.deleteSpeaker(id);
	}
}
