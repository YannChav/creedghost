package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.CompetitionSolo;
import fr.creedghost.api.model.Event;
import fr.creedghost.api.service.CompetitionSoloService;

@RestController
public class CompetitionSoloController {
	@Autowired
	private CompetitionSoloService competitionSoloService;

	@PostMapping("/competition-solo")
	public CompetitionSolo createCompetitionSolo(@RequestBody CompetitionSolo competitionSolo) {
		return competitionSoloService.saveCompetitionSolo(competitionSolo);
	}

	@GetMapping("/competition-solo/{id}")
	public CompetitionSolo getCompetitionSolo(@PathVariable("id") final Long id) {
		Optional<CompetitionSolo> cs = competitionSoloService.getCompetitionSolo(id);
		if (cs.isPresent()) {
			return cs.get();
		} else {
			return null;
		}
	}

	@GetMapping("/competitions-solo/event/{id}")
	public Iterable<CompetitionSolo> getCompetitionsSoloByEvent(@PathVariable("id") final Long idEvent) {
		Event event = new Event();
		event.setId(idEvent);

		return competitionSoloService.getCompetitionsSoloByEvent(event);
	}

	@GetMapping("/competitions-solo")
	public Iterable<CompetitionSolo> getCompetitionsSolo() {
		return competitionSoloService.getCompetitionsSolo();
	}

	@PutMapping("/competition-solo")
	public CompetitionSolo updateCompetitionSolo(@RequestBody CompetitionSolo competitionSolo) {
		Optional<CompetitionSolo> cs = competitionSoloService.getCompetitionSolo(competitionSolo.getId());
		if (cs.isPresent()) {
			return competitionSoloService.saveCompetitionSolo(competitionSolo);
		} else {
			return null;
		}
	}

	@DeleteMapping("/competition-solo/{id}")
	public void deleteCompetitionSolo(@PathVariable final Long id) {
		competitionSoloService.deleteCompetitionSolo(id);
	}
}
