package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.Activity;
import fr.creedghost.api.model.Event;
import fr.creedghost.api.service.ActivityService;

@RestController
public class ActivityController {
	@Autowired
	private ActivityService activityService;

	@PostMapping("/activity")
	public Activity createActivity(@RequestBody Activity activity) {
		return activityService.saveActivity(activity);
	}

	@GetMapping("/activity/{id}")
	public Activity getActivity(@PathVariable("id") final Long id) {
		Optional<Activity> activity = activityService.getActivity(id);
		if (activity.isPresent()) {
			return activity.get();
		} else {
			return null;
		}
	}

	@GetMapping("/activities/event/{id}")
	public Iterable<Activity> getActivitiesByEvent(@PathVariable("id") final Long idEvent) {
		Event event = new Event();
		event.setId(idEvent);

		return activityService.getActivitiesByEvent(event);
	}

	@GetMapping("/activities")
	public Iterable<Activity> getActivities() {
		return activityService.getActivities();
	}

	@PutMapping("/activity")
	public Activity updateActivity(@RequestBody Activity activity) {
		Optional<Activity> a = activityService.getActivity(activity.getId());
		if (a.isPresent()) {
			return activityService.saveActivity(activity);
		} else {
			return null;
		}
	}

	@DeleteMapping("/activity/{id}")
	public void deleteActivity(@PathVariable("id") final Long id) {
		activityService.deleteActivity(id);
	}
}
