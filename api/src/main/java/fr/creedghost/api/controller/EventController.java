package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.Event;
import fr.creedghost.api.service.EventService;

@RestController
public class EventController {
	@Autowired
	private EventService eventService;

	@PostMapping("/event")
	public Event createEvent(@RequestBody Event event) {
		return eventService.saveEvent(event);
	}

	@GetMapping("/event/{id}")
	public Event getEvent(@PathVariable("id") final Long id) {
		Optional<Event> e = eventService.getEvent(id);
		if (e.isPresent()) {
			return e.get();
		} else {
			return null;
		}
	}

	@GetMapping("/events")
	public Iterable<Event> getEvents() {
		return eventService.getEvents();
	}

	@PutMapping("/event")
	public Event updateEvent(@RequestBody Event event) {
		Optional<Event> e = eventService.getEvent(event.getId());
		if (e.isPresent()) {
			return eventService.saveEvent(event);
		} else {
			return null;
		}
	}

	@DeleteMapping("/event/{id}")
	public void deleteEvent(@PathVariable("id") final Long id) {
		eventService.deleteEvent(id);
	}
}
