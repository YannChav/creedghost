package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.CompetitionTeam;
import fr.creedghost.api.model.Event;
import fr.creedghost.api.service.CompetitionTeamService;

@RestController
public class CompetitionTeamController {
	@Autowired
	private CompetitionTeamService competitionTeamService;

	@PostMapping("/competition-team")
	public CompetitionTeam createCompetitionTeam(@RequestBody CompetitionTeam competitionTeam) {
		return competitionTeamService.saveCompetitionTeam(competitionTeam);
	}

	@GetMapping("/competition-team/{id}")
	public CompetitionTeam getCompetitionTeam(@PathVariable("id") final Long id) {
		Optional<CompetitionTeam> ct = competitionTeamService.getCompetitionTeam(id);
		if (ct.isPresent()) {
			return ct.get();
		} else {
			return null;
		}
	}

	@GetMapping("/competitions-team/event/{id}")
	public Iterable<CompetitionTeam> getCompetitionsTeamByEvent(@PathVariable("id") final Long idEvent) {
		Event event = new Event();
		event.setId(idEvent);

		return competitionTeamService.getCompetitionsTeamByEvent(event);
	}

	@GetMapping("/competitions-team")
	public Iterable<CompetitionTeam> getCompetitionsTeam() {
		return competitionTeamService.getCompetitionsTeam();
	}

	@PutMapping("/competition-team")
	public CompetitionTeam updateCompetitionTeam(@RequestBody CompetitionTeam competitionTeam) {
		Optional<CompetitionTeam> ct = competitionTeamService.getCompetitionTeam(competitionTeam.getId());
		if (ct.isPresent()) {
			return competitionTeamService.saveCompetitionTeam(competitionTeam);
		} else {
			return null;
		}
	}

	@DeleteMapping("/competition-team/{id}")
	public void deleteCompetitionTeam(@PathVariable("id") final Long id) {
		competitionTeamService.DeleteCompetitionTeam(id);
	}
}
