package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.CompetitionTeam;
import fr.creedghost.api.model.CompetitionTeamTeam;
import fr.creedghost.api.model.Team;
import fr.creedghost.api.service.CompetitionTeamTeamService;

@RestController
public class CompetitionTeamTeamController {
	@Autowired
	private CompetitionTeamTeamService competitionTeamTeamService;

	@PostMapping("/competition-team-team")
	public CompetitionTeamTeam createCompetitionTeamTeam(@RequestBody CompetitionTeamTeam competitionTeamTeam) {
		return competitionTeamTeamService.saveCompetitionTeamTeam(competitionTeamTeam);
	}

	@GetMapping("/competition-team-team/{id}")
	public CompetitionTeamTeam getCompetitionTeamTeam(@PathVariable("id") final Long id) {
		Optional<CompetitionTeamTeam> ctt = competitionTeamTeamService.getCompetitionTeamTeam(id);
		if (ctt.isPresent()) {
			return ctt.get();
		} else {
			return null;
		}
	}

	@GetMapping("/competition-team-teams/team/{id}")
	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeamsByTeam(@PathVariable("id") final Long idTeam) {
		Team team = new Team();
		team.setId(idTeam);

		return competitionTeamTeamService.getCompetitionTeamTeamsByTeam(team);
	}

	@GetMapping("/competition-team-teams/competition/{id}")
	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeamsByCompetition(
			@PathVariable("id") final Long idCompetition) {
		CompetitionTeam competitionTeam = new CompetitionTeam();
		competitionTeam.setId(idCompetition);

		return competitionTeamTeamService.getCompetitionTeamTeamsByCompetition(competitionTeam);
	}

	@GetMapping("/competition-team-teams")
	public Iterable<CompetitionTeamTeam> getCompetitionTeamTeams() {
		return competitionTeamTeamService.getCompetitionTeamTeams();
	}

	@PutMapping("/competition-team-team")
	public CompetitionTeamTeam updateCompetitionTeamTeam(@RequestBody CompetitionTeamTeam competitionTeamTeam) {
		Optional<CompetitionTeamTeam> ctt = competitionTeamTeamService
				.getCompetitionTeamTeam(competitionTeamTeam.getId());
		if (ctt.isPresent()) {
			return competitionTeamTeamService.saveCompetitionTeamTeam(competitionTeamTeam);
		} else {
			return null;
		}
	}

	@DeleteMapping("/competition-team-team/{id}")
	public void deleteCompetitionTeamTeam(@PathVariable final Long id) {
		competitionTeamTeamService.deleteCompetitionTeamTeam(id);
	}
}
