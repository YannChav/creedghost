package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.Player;
import fr.creedghost.api.service.PlayerService;

@RestController
public class PlayerController {
	@Autowired
	private PlayerService playerService;

	@PostMapping("/player")
	public Player createPlayer(@RequestBody Player player) {
		return playerService.savePlayer(player);
	}

	@GetMapping("/player/{id}")
	public Player getPlayer(@PathVariable("id") final Long id) {
		Optional<Player> p = playerService.getPlayer(id);
		if (p.isPresent()) {
			return p.get();
		} else {
			return null;
		}
	}

	@GetMapping("/player/username/{username}")
	public Player getPlayerByUsername(@PathVariable("username") final String username) {
		Optional<Player> p = playerService.getPlayerByUsername(username);
		if (p.isPresent()) {
			return p.get();
		} else {
			return null;
		}
	}

	@GetMapping("/players")
	public Iterable<Player> getPlayers() {
		return playerService.getPlayers();
	}

	@PutMapping("/player")
	public Player updatePlayer(@RequestBody Player player) {
		Optional<Player> p = playerService.getPlayer(player.getId());
		if (p.isPresent()) {
			return playerService.savePlayer(player);
		} else {
			return null;
		}
	}

	@DeleteMapping("/player/{id}")
	public void deletePlayer(@PathVariable("id") final Long id) {
		playerService.deletePlayer(id);
	}
}
