package fr.creedghost.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.creedghost.api.model.Team;
import fr.creedghost.api.service.TeamService;

@RestController
public class TeamController {
	@Autowired
	private TeamService teamService;

	@PostMapping("/team")
	public Team createTeam(@RequestBody Team team) {
		return teamService.saveTeam(team);
	}

	@GetMapping("/team/{id}")
	public Team getTeam(@PathVariable("id") final Long id) {
		Optional<Team> t = teamService.getTeam(id);
		if (t.isPresent()) {
			return t.get();
		} else {
			return null;
		}
	}

	@GetMapping("/team/name/{name}")
	public Team getTeamByName(@PathVariable("name") final String name) {
		Optional<Team> t = teamService.getTeamByName(name);
		if (t.isPresent()) {
			return t.get();
		} else {
			return null;
		}
	}

	@GetMapping("/team/player/{id}")
	public Iterable<Team> getTeamsByPlayer(@PathVariable("id") int idPlayer) {
		return teamService.getTeamsByPlayerSQL(idPlayer);
	}

	@GetMapping("/teams")
	public Iterable<Team> getTeams() {
		return teamService.getTeams();
	}

	@PutMapping("/team")
	public Team updateTeam(@RequestBody Team team) {
		Optional<Team> t = teamService.getTeam(team.getId());
		if (t.isPresent()) {
			return teamService.saveTeam(team);
		} else {
			return null;
		}
	}

	@DeleteMapping("/team/{id}")
	public void deleteTeam(@PathVariable("id") final Long id) {
		teamService.deleteTeam(id);
	}
}
