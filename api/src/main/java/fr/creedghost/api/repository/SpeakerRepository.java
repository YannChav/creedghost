package fr.creedghost.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.creedghost.api.model.Speaker;

@Repository
public interface SpeakerRepository extends JpaRepository<Speaker, Long> {

	Optional<Speaker> findByEmail(String email);
}
