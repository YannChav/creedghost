package fr.creedghost.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.creedghost.api.model.Conference;
import fr.creedghost.api.model.Event;

@Repository
public interface ConferenceRepository extends JpaRepository<Conference, Long> {

	List<Conference> findByEvent(Event event);
}
