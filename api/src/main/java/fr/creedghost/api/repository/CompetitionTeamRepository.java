package fr.creedghost.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.creedghost.api.model.CompetitionTeam;
import fr.creedghost.api.model.Event;

@Repository
public interface CompetitionTeamRepository extends JpaRepository<CompetitionTeam, Long> {

	List<CompetitionTeam> findByEvent(Event event);
}
