package fr.creedghost.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.creedghost.api.model.Event;
import fr.creedghost.api.model.Stand;

@Repository
public interface StandRepository extends JpaRepository<Stand, Long> {
	List<Stand> findByEvent(Event event);
}
