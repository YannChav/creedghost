package fr.creedghost.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.creedghost.api.model.Event;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

}
