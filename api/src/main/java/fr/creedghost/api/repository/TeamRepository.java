package fr.creedghost.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.creedghost.api.model.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
	Optional<Team> findByName(String name);

	@Query(value = "SELECT * FROM team INNER JOIN player_team ON team.id = player_team.team_id WHERE player_team.player_id = :idPlayer", nativeQuery = true)
	public Iterable<Team> findByPlayerSQL(@Param("idPlayer") int idPlayer);
}
