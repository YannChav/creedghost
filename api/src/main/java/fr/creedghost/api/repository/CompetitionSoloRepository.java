package fr.creedghost.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.creedghost.api.model.CompetitionSolo;
import fr.creedghost.api.model.Event;

@Repository
public interface CompetitionSoloRepository extends JpaRepository<CompetitionSolo, Long> {

	List<CompetitionSolo> findByEvent(Event event);
}
