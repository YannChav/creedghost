package fr.creedghost.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.creedghost.api.model.Activity;
import fr.creedghost.api.model.Event;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

	Iterable<Activity> findByEvent(Event event);

}
