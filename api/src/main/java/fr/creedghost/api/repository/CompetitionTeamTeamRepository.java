package fr.creedghost.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.creedghost.api.model.CompetitionTeam;
import fr.creedghost.api.model.CompetitionTeamTeam;
import fr.creedghost.api.model.Team;

@Repository
public interface CompetitionTeamTeamRepository extends JpaRepository<CompetitionTeamTeam, Long> {

	List<CompetitionTeamTeam> findByTeam(Team team);

	List<CompetitionTeamTeam> findByCompetition(CompetitionTeam competition);

}
