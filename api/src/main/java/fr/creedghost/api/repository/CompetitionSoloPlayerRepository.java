package fr.creedghost.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.creedghost.api.model.CompetitionSolo;
import fr.creedghost.api.model.CompetitionSoloPlayer;
import fr.creedghost.api.model.Player;

@Repository
public interface CompetitionSoloPlayerRepository extends JpaRepository<CompetitionSoloPlayer, Long> {

	List<CompetitionSoloPlayer> findByPlayer(Player player);

	List<CompetitionSoloPlayer> findByCompetition(CompetitionSolo competition);
}
